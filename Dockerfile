FROM ubuntu:bionic as build

ENV GOLANG_VERSION 1.13.6


RUN apt-get update
RUN apt-get install -y build-essential curl git 

RUN curl -sSL https://storage.googleapis.com/golang/go$GOLANG_VERSION.linux-amd64.tar.gz \
		| tar -v -C /usr/local -xz

ENV PATH /usr/local/go/bin:$PATH

RUN mkdir -p /go/src /go/src/app /go/bin && chmod -R 777 /go
ENV GOROOT /usr/local/go
ENV GOPATH /go
ENV PATH /go/bin:$PATH

WORKDIR /go/src/app

COPY . .

RUN make dev-prepare
RUN make engine


FROM ubuntu:bionic

COPY --from=build /go /go
COPY --from=build /usr/bin/make /usr/bin/make
COPY --from=build /usr/local/go /usr/local/go

RUN apt-get update
RUN apt-get install -y ca-certificates

ENV PATH /usr/local/go/bin:$PATH


ENV GOROOT /usr/local/go
ENV GOPATH /go
ENV PATH /go/bin:$PATH

WORKDIR /go/src/app

EXPOSE 8080


ENTRYPOINT ["/go/src/app/cmd/entrypoint.sh"]