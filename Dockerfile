FROM ubuntu:bionic as build

ARG DEVEL=no
ARG IPYTHON=no

ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
    apt-get install -y build-essential \
                       libhiredis-dev \
                       libssl-dev \
                       libcurl4-openssl-dev \
                       libxml2-dev \
                       libxslt1-dev \
                       python3-all-dev \
                       python3-venv \
                       postgresql-client \
                       postgresql-server-dev-all

RUN python3 -m venv /env

ENV PATH "/env/bin:$PATH"

RUN pip install --upgrade pip setuptools wheel

COPY requirements/*.txt /tmp/requirements/

RUN if [ "x$DEVEL" = "xyes" ]; then pip install -r /tmp/requirements/dev.txt -r /tmp/requirements/testing.txt -r /tmp/requirements/linting.txt; fi

RUN if [ "x$DEVEL" = "xyes" ] && [ "x$IPYTHON" = "xyes" ]; then pip install ipython; fi

RUN pip install -r /tmp/requirements/deploy.txt -r /tmp/requirements/main.txt


FROM ubuntu:bionic

ARG DEVEL=no

ENV PYTHONUNBUFFERED 1
ENV BOOK_ENV production

RUN adduser --system --ingroup users book

RUN apt-get update && \
    apt-get install -y ca-certificates \
                       libhiredis0.13 \
                       python3-all \
                       libxml2 \
                       libxslt1.1 \
                       postgresql-client \
                       uwsgi \
                       uwsgi-plugin-python3 && \
    apt-get clean

COPY --from=build /env /env
COPY . /app

WORKDIR /app

ENV PATH "/env/bin:$PATH"

RUN pip install --no-deps -e .

USER book:users

ENV LC_ALL "C.UTF-8"
ENV LANG "C.UTF-8"

ENTRYPOINT ["/app/bin/entrypoint.bash"]
CMD ["uwsgi"]
