variable "vpc_cidr_block" {
  type = string
}

variable "vpc_az" {
  type = list(string)
}

variable "eks_cluster_name" {
	type = string
	default = "book-eks-cluster"
}

