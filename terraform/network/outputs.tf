output "vpc_id" {
  value = aws_vpc.book.id
}

output "vpc_cidr_block" {
  value = aws_vpc.book.cidr_block
}

output "subnets" {
  value = aws_subnet.book.*.id
}

