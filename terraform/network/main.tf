resource "aws_vpc" "book" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true
  enable_dns_support   = true
  instance_tenancy     = "default"

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    "Name" = "${var.eks_cluster_name}"
  }
}

resource "aws_subnet" "book" {
  count             = length(var.vpc_az)
  cidr_block        = cidrsubnet(var.vpc_cidr_block, 8, count.index)
  availability_zone = var.vpc_az[count.index]
  vpc_id            = aws_vpc.book.id

  tags = {
    "Name" = var.eks_cluster_name
  }
}

