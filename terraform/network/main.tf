resource "aws_vpc" "book" {
  cidr_block           = var.vpc_cidr_block
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    "Name" = "Book"
  }
}


resource "aws_subnet" "book" {
  cidr_block = cidrsubnet(var.vpc_cidr_block, 8, 1)
  vpc_id     = aws_vpc.book.id

  tags = {
    "Name" = "book"
  }
}

resource "aws_internet_gateway" "book" {
  vpc_id = aws_vpc.book.id
  tags = {
    "Name" = "book"
  }
}

resource "aws_route_table" "book" {
  vpc_id = aws_vpc.book.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.book.id
  }
}

resource "aws_route_table_association" "book" {
  subnet_id      = aws_subnet.book.id
  route_table_id = aws_route_table.book.id

}

resource "aws_security_group" "book" {
  name        = "book"
  description = "Communication with world"
  vpc_id      = aws_vpc.book.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "Name" = "book"
  }
}
