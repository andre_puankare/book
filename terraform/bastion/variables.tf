variable "eks_cluster_name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "subnets" {
  type = list(string)
}

variable "internal_networks" {
  type = list(string)
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "disk_size" {
  type    = number
  default = 10
}

variable "hostname" {
  type = string
}

