provider "aws" {
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
  region     = var.aws_region
}


module "network" {
  source           = "./network"
  vpc_cidr_block   = var.vpc_cidr_block
  vpc_az           = var.vpc_az
  eks_cluster_name = var.eks_cluster_name
}

module "rds-test" {
  source = "./rds"
  name   = "test"

  eks_cluster_name  = var.eks_cluster_name
  vpc_id            = module.network.vpc_id
  vpc_cidr_block    = module.network.vpc_cidr_block
  subnets           = module.network.subnets
  identifier        = var.rds_identifier
  storage_type      = var.rds_storage_type
  allocated_storage = var.rds_allocated_storage["test"]
  engine            = var.rds_engine
  engine_version    = var.rds_engine_version
  instance_class    = var.rds_instance_class["test"]
  db_username       = var.rds_db_username
  db_password       = var.rds_test_db_password
}

module "bastion" {
  source            = "./bastion"
  eks_cluster_name  = var.eks_cluster_name
  vpc_id            = module.network.vpc_id
  subnets           = module.network.subnets
  internal_networks = [module.network.vpc_cidr_block]
  hostname          = var.bastion_hostname
}
