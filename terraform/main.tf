provider "aws" {
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
  region     = var.aws_region
}

module "network" {
  source = "./network"
}

module "repos" {
  source = "./ecr"
}

module "dns" {
  source = "./dns"
}

resource "aws_instance" "book" {
  ami                         = "ami-0cc0a36f626a4fdf5"
  instance_type               = "t2.micro"
  subnet_id                   = module.network.subnet_id
  associate_public_ip_address = true
  key_name                    = "book"
  vpc_security_group_ids      = [module.network.aws_security_group_book_id]
  root_block_device {
    volume_type           = "standard"
    volume_size           = 33
    delete_on_termination = true
  }
  tags = {
    Name = "book"
  }
}

resource "aws_route53_record" "book" {
  zone_id = module.dns.zone_id
  name    = "www"
  type    = "A"
  ttl     = 300
  records = [aws_instance.book.public_ip]
}
