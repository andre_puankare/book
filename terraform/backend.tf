terraform {
  backend "s3" {
    bucket = "book-terraform-state"
    key    = "tfstate"
    region = "eu-central-1"
  }
}

