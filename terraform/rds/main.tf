variable "name" {}
data "aws_availability_zones" "available" {}

resource "aws_db_subnet_group" "book" {
  name       = "${var.eks_cluster_name}-${var.name}"
  subnet_ids = var.subnets

  tags = {
    "Name" = "${var.eks_cluster_name}-${var.name}"
  }
}

resource "aws_security_group" "book-rds" {
  name        = "${var.eks_cluster_name}-rds-${var.name}"
  description = "Allow communication with RDS"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  tags = {
    "Name" = "${var.eks_cluster_name}-rds-${var.name}"
  }
}

resource "aws_db_instance" "book-db" {
  identifier        = "${var.identifier}-${var.name}"
  storage_type      = var.storage_type
  allocated_storage = var.allocated_storage
  engine            = var.engine
  engine_version    = var.engine_version
  instance_class    = var.instance_class
  name              = "book"
  username          = var.db_username
  password          = var.db_password

  allow_major_version_upgrade = true
  auto_minor_version_upgrade  = true
  apply_immediately           = true

  vpc_security_group_ids = [
    aws_security_group.book-rds.id,
  ]

  db_subnet_group_name = aws_db_subnet_group.book.id
  deletion_protection  = true
  storage_encrypted    = false
  skip_final_snapshot  = true
  publicly_accessible  = false
  multi_az             = false

  tags = {
    "Name" = "${var.eks_cluster_name}-${var.name}"
  }
}

