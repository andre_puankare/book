variable "aws_access_key_id" {
	type = string
}

variable "aws_secret_access_key" {
	type = string
}

variable "aws_region" {
	type = string
	default = "eu-central-1"
}

variable "vpc_az" {
  default = [
    "eu-central-1a",
    "eu-central-1b",
    "eu-central-1c",
    "eu-central-1d",
    "eu-central-1f",
  ]
  type = list(string)
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.20.0.0/16"
}

variable "eks_cluster_name" {
  type    = string
  default = "book-k8s"
}

variable "rds_storage_type" {
  type    = string
  default = "gp2"
}

variable "rds_allocated_storage" {
  type = map(string)

  default = {
    prod = "100"
    test = "20"
  }
}

variable "rds_engine" {
  type    = string
  default = "postgres"
}

variable "rds_engine_version" {
  type    = string
  default = "11.5"
}

variable "rds_instance_class" {
  type = map(string)
  default = {
    prod = "db.t2.small"
    test = "db.t2.micro"
  }
}

variable "rds_identifier" {
  type    = string
  default = "book-db"
}

variable "rds_db_username" {
  type    = string
  default = "book"
}

variable "rds_prod_db_password" {
  type = string
}

variable "rds_test_db_password" {
  type = string
}

variable "ec2_instance_type" {
  type    = string
  default = "t2.micro"
}

variable "bastion_hostname" {
  type    = string
  default = "bastion.book.com"
}

