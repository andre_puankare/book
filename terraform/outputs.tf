output "bastion_host" {
  value = module.bastion.public_dns
}

output "rds_test_host" {
  value = module.rds-test.address
}

