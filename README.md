# Installation
```shell script
python3.6 -m venv env                         # Create virtualenv
source env/bin/activate                       # Acivate virtual environments
pip install -U pip setuptools wheel            # Upgrade pip
pip install -e ."[dev,testing,linting,deploy]" # Install book in development mode
pip install docker-compose
```

# book CLI
```shell script
book serve --reload                        # Run debug waitress server with reloading when code is change
book shell                                     # Run book shell
book db revision -a                            # Automatic detect changes on model and create migrations
book db upgrade head                           # Apply migrations 
```

# Docker Compose
```shell script
./dev/serve
./dev/alembic.bash upgrade head
```