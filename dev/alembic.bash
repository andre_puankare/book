#!/bin/bash

exec docker-compose run -u $(id -u) --rm web db "$@"
