#!/bin/bash

NAME="book/$(python setup.py --name)"

function ensure_clean {
    if ! git diff --quiet; then
        >&2  echo "Working tree is dirty, unable to proceed."
        exit 99
    fi
}

case "$1" in
    commit)
        ensure_clean
        BRANCH="$(git rev-parse --abbrev-ref HEAD)"
        COMMIT="$(git rev-parse --short=12 HEAD)"
        VERSION="$COMMIT-${BRANCH//[^a-zA-Z_0-9]/-}"
    ;;
    commit-only)
        ensure_clean
        VERSION="$(git rev-parse --short=12 HEAD)"
    ;;
    time)
        VERSION="$(date -u +'%Y%m%d%H%M')"
    ;;
    version)
        ensure_clean
        VERSION="v$(python setup.py --version)"
    ;;
    *)
        >&2 echo "Invalid version format. Available formats: commit, commit-only, time, version"
        exit 98
    ;;
esac

shift

TAG="$NAME:$VERSION"
TAG_LATEST="$NAME:latest"
REGISTRY="994844136676.dkr.ecr.eu-central-1.amazonaws.com"

set -e

docker build --build-arg DEVEL="${DEVEL:-no}" \
             -t "$TAG" -t "$TAG_LATEST" \
             -t "$REGISTRY/$TAG" -t "$REGISTRY/$TAG_LATEST" \
             $@ .

docker push "$REGISTRY/$TAG"
docker push "$REGISTRY/$TAG_LATEST"
