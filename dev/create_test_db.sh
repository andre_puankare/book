#!/bin/sh

POSTGRES_USER="${POSTGRES_USER:-postgres}"
POSTGRES_TEST_DB="${POSTGRES_DB:-${POSTGRES_USER}}_test"

exec psql -v ON_ERROR_STOP=1 \
     --username "$POSTGRES_USER" \
     --no-password \
     --dbname postgres \
     --set test_db="$POSTGRES_TEST_DB" \
     --set user="$POSTGRES_USER" \
     <<-'EOSQL'
DROP DATABASE IF EXISTS :"test_db";
CREATE DATABASE :"test_db" WITH OWNER :"user";
EOSQL
