package rest

import (
	"net/http"
	"github.com/labstack/echo"
)

// NewHealthHandler register handlers for certain routes
func NewHealthHandler(
	e *echo.Echo,
) {
	// ok
	e.GET("/", ok)
}

// healthHandler  represent health
type healthHandler struct {
}

// Handler
func ok(c echo.Context) error {
	return c.String(http.StatusOK, "ok")
}
