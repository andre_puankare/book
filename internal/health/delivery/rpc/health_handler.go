package rpc

import (
	"context"
	"log"
	"net"

	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

// server is used to implement helloworld.GreeterServer.
type server struct {
	UnimplementedHealthServer
}

// Health implements rpc.Health
func (s *server) Echo(ctx context.Context, in *HealthRequest) (*HealthReply, error) {
	log.Printf("Received: %v", in.GetEcho())
	return &HealthReply{Echo: in.GetEcho()}, nil
}

func RunGrpc() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	RegisterHealthServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
