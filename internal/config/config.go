package config

import (
	"fmt"

	"github.com/spf13/viper"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// Config represents an application configuration.
type Config struct {
	APIDomain string
	// the server port. Defaults to 8080
	ServerPort int
	// the data source name (DSN) for connecting to the database. required.
	DBURI string
	// JWT signing key. required.
	JWTSigningKey string
	// JWT verification key. required.
	JWTVerificationKey string
	// JWT expiration in hours. Defaults to 72 hours (3 days)
	JWTExpiration int
	// Google app credentials
	GoogleOauthConfig *oauth2.Config
	// Google oauth state string
	GoogleOauthStateString string
}

// Load returns an application configuration which is populated from the given configuration file and environment variables.
func Load() (*Config, error) {
	// Server
	serverPort := viper.GetInt("SERVER_PORT")

	// Database
	username := viper.GetString("DB_USER")
	password := viper.GetString("DB_PASS")
	dbName := viper.GetString("DB_NAME")
	dbHost := viper.GetString("DB_HOST")
	dbURI := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, username, dbName, password)

	// Domain
	apiDomain := viper.GetString("API_DOMAIN")
	googleRedirectURL := viper.GetString("GOOGLE_REDIRECT_URL")

	// Google app
	googleAppClientID := viper.GetString("GOOGLE_APP_CLIENT_ID")
	googleAppSecret := viper.GetString("GOOGLE_APP_SECRET")
	googleOauthStateString := viper.GetString("GOOGLE_OAUTH_STATE_STRING")

	// default config
	config := Config{
		DBURI:      dbURI,
		ServerPort: serverPort,
		APIDomain:  apiDomain,
		GoogleOauthConfig: &oauth2.Config{
			RedirectURL:  googleRedirectURL,
			ClientID:     googleAppClientID,
			ClientSecret: googleAppSecret,
			Scopes: []string{
				"https://www.googleapis.com/auth/userinfo.profile",
				"https://www.googleapis.com/auth/userinfo.email",
				"https://www.googleapis.com/auth/calendar.events.readonly",
				"https://www.googleapis.com/auth/calendar.readonly",
				"https://www.googleapis.com/auth/contacts.readonly",
			},
			Endpoint: google.Endpoint,
		},
		GoogleOauthStateString: googleOauthStateString,
	}

	return &config, nil
}

func init() {
	viper.AutomaticEnv()
	viper.SetDefault("SERVER_PORT", 8080)
	viper.SetDefault("DB_USER", "book")
	viper.SetDefault("DB_PASS", "book")
	viper.SetDefault("DB_NAME", "book")
	viper.SetDefault("DB_HOST", "localhost")
	viper.SetDefault("GOOGLE_OAUTH_STATE_STRING", "g2C+EGa3Zeg0bUBrsrSKS/TF2sZDNPPVuuKIcx7gKMY=")
}
