package usecases

import (
	"context"

	"bitbucket.org/andre_puankare/book/internal/account/types"

	repos "bitbucket.org/andre_puankare/book/internal/account/repositories"
)

// AuthUsecase encapsulates usecase logic for accounts.
type AuthUsecase interface {
	GetUserProfile(ctx context.Context, state string, code string) (types.GoogleProfile, error)
	GetLoginRedirectURL() string
}

// NewGoogleAuthUsecase create new google usecase
func NewGoogleAuthUsecase(googleAuthRepo repos.AuthRepo) AuthUsecase {
	return googleAuthUsecase{
		GoogleAuthRepo: googleAuthRepo,
	}
}

// GoogleAuthUsecase represent google usecase for login, get user info
type googleAuthUsecase struct {
	GoogleAuthRepo repos.AuthRepo
}

// GetLoginRedirectURL redirect to login
func (u googleAuthUsecase) GetLoginRedirectURL() string {
	return u.GoogleAuthRepo.GetLoginRedirectURL()
}

// GetUserInfo returns user info
func (u googleAuthUsecase) GetUserProfile(
	ctx context.Context,
	state string,
	code string,
) (types.GoogleProfile, error) {
	googleProfile, err := u.GoogleAuthRepo.GetUserProfile(ctx, state, code)
	if err != nil {
		return types.GoogleProfile{}, err
	}
	return googleProfile, nil
}
