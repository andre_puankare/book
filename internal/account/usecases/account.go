package usecases

import (
	"bitbucket.org/andre_puankare/book/internal/account/entities"
	repos "bitbucket.org/andre_puankare/book/internal/account/repositories"
	"bitbucket.org/andre_puankare/book/internal/account/types"

	"context"
)

// AccountUsecase encapsulates usecase logic for accounts.
type AccountUsecase interface {
	GetByID(ctx context.Context, id uint) (types.AccountResponse, error)
	GetByUsername(ctx context.Context, username string) (types.AccountResponse, error)
	Query(ctx context.Context, offset, limit int) ([]entities.Account, error)
	Count(ctx context.Context) (int, error)
	Create(ctx context.Context, input types.CreateAccountRequest) (types.AccountResponse, error)
	Update(ctx context.Context, id string, input types.CreateAccountRequest) (entities.Account, error)
	Delete(ctx context.Context, id string) (entities.Account, error)
}

type accountUsecase struct {
	accountRepo repos.AccountRepository
}

// NewAccountUsecase creates a new account service.
func NewAccountUsecase(accountRepo repos.AccountRepository) AccountUsecase {
	return accountUsecase{accountRepo}
}

func (s accountUsecase) GetByUsername(ctx context.Context, username string) (types.AccountResponse, error) {
	acc, err := s.accountRepo.GetByUsername(ctx, username)
	if err != nil {
		return types.AccountResponse{}, err
	}

	return types.AccountResponse{
		ID:        acc.ID,
		CreatedAt: acc.CreatedAt,
		UpdatedAt: acc.UpdatedAt,
		Username:  acc.Username,
		FirstName: acc.FirstName,
		LastName:  acc.LastName,
		Email:     acc.Email,
		Phone:     acc.Phone,
		Bio:       acc.Bio,
		IsAdmin:   acc.IsAdmin,
		IsActive:  acc.IsActive,
	}, nil
}

// Get returns the account with the specified the account ID.
func (s accountUsecase) GetByID(ctx context.Context, id uint) (types.AccountResponse, error) {
	acc, err := s.accountRepo.GetByID(ctx, id)
	if err != nil {
		return types.AccountResponse{}, err
	}

	return types.AccountResponse{
		ID:        acc.ID,
		CreatedAt: acc.CreatedAt,
		UpdatedAt: acc.UpdatedAt,
		Username:  acc.Username,
		FirstName: acc.FirstName,
		LastName:  acc.LastName,
		Email:     acc.Email,
		Phone:     acc.Phone,
		Bio:       acc.Bio,
		IsAdmin:   acc.IsAdmin,
		IsActive:  acc.IsActive,
	}, nil
}

// Create creates a new account.
func (s accountUsecase) Create(ctx context.Context, req types.CreateAccountRequest) (types.AccountResponse, error) {
	acc, err := s.accountRepo.Create(ctx, entities.Account{
		Username:  req.Username,
		FirstName: req.FirstName,
		LastName:  req.LastName,
		Password:  req.Password,
		Email:     req.Email,
		Phone:     req.Phone,
		Bio:       req.Bio,
	})
	return types.AccountResponse{
		ID:        acc.ID,
		CreatedAt: acc.CreatedAt,
		UpdatedAt: acc.UpdatedAt,
		Username:  acc.Username,
		FirstName: acc.FirstName,
		LastName:  acc.LastName,
		Email:     acc.Email,
		Phone:     acc.Phone,
		Bio:       acc.Bio,
		IsAdmin:   acc.IsAdmin,
		IsActive:  acc.IsActive,
	}, err
}

// Update updates the album with the specified ID.
func (s accountUsecase) Update(ctx context.Context, id string, req types.CreateAccountRequest) (entities.Account, error) {
	return entities.Account{}, nil
}

// Delete deletes the album with the specified ID.
func (s accountUsecase) Delete(ctx context.Context, id string) (entities.Account, error) {
	return entities.Account{}, nil
}

// Count returns the number of albums.
func (s accountUsecase) Count(ctx context.Context) (int, error) {
	return 0, nil
}

// Query returns the albums with the specified offset and limit.
func (s accountUsecase) Query(ctx context.Context, offset, limit int) ([]entities.Account, error) {

	return []entities.Account{}, nil
}
