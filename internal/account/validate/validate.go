package validate

import (
	"bitbucket.org/andre_puankare/book/internal/account/types"
	"bitbucket.org/andre_puankare/book/pkg/utils/delivery/rest"
	validator "gopkg.in/go-playground/validator.v9"
)

// ValidateAccountRequest validate AccountRequest structure
func ValidateAccountRequest(m *types.CreateAccountRequest) (bool, rest.ValidationResponse) {
	validate := validator.New()
	err := validate.Struct(m)

	resp := rest.ValidationResponse{}
	if err != nil {
		resp = resp.FromValidationErrors(err, "body")
		return false, resp
	}
	return true, resp
}

// ValidateUsernameRequest validate UsernameRequest structure
func ValidateUsernameRequest(m *types.UsernameRequest) (bool, rest.ValidationResponse) {
	validate := validator.New()
	err := validate.Struct(m)

	resp := rest.ValidationResponse{}
	if err != nil {
		resp = resp.FromValidationErrors(err, "body")
		return false, resp
	}
	return true, resp
}
