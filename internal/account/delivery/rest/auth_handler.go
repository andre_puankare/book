package rest

import (
	"context"
	"net/http"

	"bitbucket.org/andre_puankare/book/internal/account/usecases"
	_ "bitbucket.org/andre_puankare/book/pkg/utils/delivery/rest"
	"github.com/labstack/echo"
)

// NewAuthHandler register handlers for certain routes
func NewAuthHandler(
	e *echo.Group,
	accUC usecases.AccountUsecase,
	googleAuthUC usecases.AuthUsecase,

) {
	handler := authHandler{
		accUC:        accUC,
		googleAuthUC: googleAuthUC,
	}

	// auth api
	e.GET("/auth/google/login", handler.GoogleLogin)
	e.GET("/auth/google/callback", handler.GoogleCallback)
}

// AccountHandler  represent the httphandler for article
type authHandler struct {
	accUC        usecases.AccountUsecase
	googleAuthUC usecases.AuthUsecase
}

func (a *authHandler) GoogleLogin(c echo.Context) error {
	return c.Redirect(
		http.StatusTemporaryRedirect,
		a.googleAuthUC.GetLoginRedirectURL(),
	)
}

func (a *authHandler) GoogleCallback(c echo.Context) error {
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	googleProfile, err := a.googleAuthUC.GetUserProfile(
		ctx,
		c.FormValue("state"),
		c.FormValue("code"),
	)
	if err != nil {
		c.Logger().Error(err)
		return c.Redirect(
			http.StatusTemporaryRedirect,
			a.googleAuthUC.GetLoginRedirectURL(),
		)
	}

	return c.JSON(http.StatusOK, googleProfile)
}
