package rest

import (
	"context"
	"net/http"
	"strconv"

	"bitbucket.org/andre_puankare/book/internal/account/types"
	"bitbucket.org/andre_puankare/book/internal/account/usecases"
	"bitbucket.org/andre_puankare/book/internal/account/validate"
	"bitbucket.org/andre_puankare/book/pkg/apperror"
	"bitbucket.org/andre_puankare/book/pkg/utils/delivery/rest"
	"github.com/labstack/echo"
)

// NewAccountHandler register handlers for certain routes
func NewAccountHandler(
	e *echo.Group,
	accUC usecases.AccountUsecase,
) {
	handler := accountHandler{
		accUC: accUC,
	}

	e.POST("/accounts/username", handler.CheckUsername)
	e.GET("/accounts/:id", handler.Get)
	e.POST("/accounts", handler.Create)
}

// AccountHandler  represent the httphandler for article
type accountHandler struct {
	accUC usecases.AccountUsecase
}

func (a *accountHandler) CheckUsername(c echo.Context) error {
	usernameRequest := new(types.UsernameRequest)
	if err := c.Bind(usernameRequest); err != nil {
		return err
	}

	if ok, err := validate.ValidateUsernameRequest(usernameRequest); !ok {
		return c.JSON(http.StatusBadRequest, err)
	}

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	acc, err := a.accUC.GetByUsername(ctx, usernameRequest.Username)
	if err != nil {
		return c.JSON(rest.GetStatusCode(err), rest.InternalServerError("Server error"))
	}
	if acc.ID <= 0 {
		return c.JSON(http.StatusOK, rest.OK("available"))
	}

	return c.JSON(http.StatusOK, rest.OK("not available"))
}

// Get will fetch the account based on given params
func (a *accountHandler) Create(c echo.Context) error {

	accountRequest := new(types.CreateAccountRequest)
	if err := c.Bind(accountRequest); err != nil {
		return err
	}

	if ok, err := validate.ValidateAccountRequest(accountRequest); !ok {
		return c.JSON(http.StatusBadRequest, err)
	}

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	acc, err := a.accUC.Create(ctx, *accountRequest)
	if err != nil {
		return c.JSON(rest.GetStatusCode(err), rest.ErrorResponse{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, acc)
}

// Get will fetch the account based on given params
func (a *accountHandler) Get(c echo.Context) error {
	idP, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusNotFound, apperror.ErrNotFound.Error())
	}

	id := uint(idP)
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	acc, err := a.accUC.GetByID(ctx, id)
	if err != nil {
		return c.JSON(rest.GetStatusCode(err), rest.ErrorResponse{Message: err.Error()})
	}
	return c.JSON(http.StatusOK, acc)
}

// Get will fetch the account based on given params
func (a *accountHandler) Update(c echo.Context) error {

	accountRequest := new(types.CreateAccountRequest)
	if err := c.Bind(accountRequest); err != nil {
		return err
	}

	if ok, err := validate.ValidateAccountRequest(accountRequest); !ok {
		return c.JSON(http.StatusBadRequest, err)
	}

	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}

	acc, err := a.accUC.Create(ctx, *accountRequest)
	if err != nil {
		return c.JSON(rest.GetStatusCode(err), rest.ErrorResponse{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, acc)
}
