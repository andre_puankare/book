package repositories

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/andre_puankare/book/internal/account/types"
	"golang.org/x/oauth2"
)

// AuthRepo represent interface to login, get user info, sync
type AuthRepo interface {
	GetUserProfile(ctx context.Context, state string, code string) (types.GoogleProfile, error)
	GetLoginRedirectURL() string
}

// NewGoogleAuthRepository creates a new google repo
func NewGoogleAuthRepository(oauthConfig *oauth2.Config, oauthStateString string) GoogleAuthRepo {
	return GoogleAuthRepo{
		OauthConfig:      oauthConfig,
		OauthStateString: oauthStateString,
	}
}

// GoogleAuthRepo represent repo struct
type GoogleAuthRepo struct {
	OauthConfig      *oauth2.Config
	OauthStateString string
}

// GetLoginRedirectURL return url to login
func (r GoogleAuthRepo) GetLoginRedirectURL() string {
	return r.OauthConfig.AuthCodeURL(
		r.OauthStateString,
	)
}

// GetUserProfile fetch user info
func (r GoogleAuthRepo) GetUserProfile(
	ctx context.Context,
	state string,
	code string,
) (types.GoogleProfile, error) {
	var googleProfile types.GoogleProfile
	if state != r.OauthStateString {
		return googleProfile, fmt.Errorf("invalid oauth state")
	}

	token, err := r.OauthConfig.Exchange(oauth2.NoContext, code)
	if err != nil {
		return googleProfile, fmt.Errorf("code exchange failed: %s", err.Error())
	}
	tok, _ := r.ExtractJwtClaims(token.Extra("id_token").(string))
	fmt.Printf("%+v", tok)
	
	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		return googleProfile, fmt.Errorf("failed getting user info: %s", err.Error())
	}
	defer response.Body.Close()

	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return googleProfile, fmt.Errorf("failed reading response body: %s", err.Error())
	}

	err = json.Unmarshal(contents, &googleProfile)
	if err != nil {
		return googleProfile, fmt.Errorf("failed decode response body: %s", err.Error())
	}

	return googleProfile, nil
}

// ExtractJwtClaims extract jwt claims
func (r GoogleAuthRepo) ExtractJwtClaims(token string) (*types.GoogleJwtClaims, error) {
	tokenStruct := &types.GoogleJwtClaims{}
	jwtParts := strings.Split(token, ".")
	out, _ := base64.RawURLEncoding.DecodeString(jwtParts[1])
	err := json.Unmarshal(out, &tokenStruct)
	if err != nil {
		return nil, err
	}

	return tokenStruct, nil
}
