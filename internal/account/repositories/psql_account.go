package repositories

import (
	"context"
	"fmt"

	"bitbucket.org/andre_puankare/book/internal/account/entities"
	"bitbucket.org/andre_puankare/book/pkg/apperror"
	"github.com/jinzhu/gorm"
	"github.com/pborman/uuid"
	"golang.org/x/crypto/bcrypt"
)

// AccountRepository encapsulates the logic to access account from the data source.
type AccountRepository interface {
	// Get returns the account with the specified account ID.
	GetByID(ctx context.Context, id uint) (entities.Account, error)
	// Get returns the account with the specified account Username.
	GetByUsername(ctx context.Context, username string) (entities.Account, error)
	// Count returns the number of account.
	Count(ctx context.Context) (int, error)
	// Query returns the list of account with the given offset and limit.
	Query(ctx context.Context, offset, limit int) ([]entities.Account, error)
	// Create saves a new account in the storage.
	Create(ctx context.Context, account entities.Account) (entities.Account, error)
	// Update updates the account with given ID in the storage.
	Update(ctx context.Context, account entities.Account) error
	// Delete removes the account with given ID from the storage.
	Delete(ctx context.Context, id string) error
}

// accountRepository persists accounts in database
type accountRepository struct {
	db *gorm.DB
}

// NewAccountRepository creates a new account repository
func NewAccountRepository(db *gorm.DB) AccountRepository {
	return accountRepository{db}
}

// GetByID reads the account with the specified ID from the database.
func (r accountRepository) GetByID(ctx context.Context, id uint) (entities.Account, error) {
	acc := &entities.Account{}
	r.db.Table("accounts").Where("id = ?", id).First(acc)

	if acc.ID <= 0 {
		return *acc, apperror.ErrNotFound
	}

	return *acc, nil
}

// GetByUsername reads the account with the specified username from the database
func (r accountRepository) GetByUsername(ctx context.Context, username string) (entities.Account, error) {
	acc := &entities.Account{}
	r.db.Table("accounts").Where("username = ?", username).First(acc)

	return *acc, nil
}

// Create saves a new account record in the database.
// It returns the ID of the newly inserted albaccountum record.
func (r accountRepository) Create(ctx context.Context, account entities.Account) (entities.Account, error) {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(account.Password), bcrypt.DefaultCost)
	account.Password = string(hashedPassword)
	account.UUID = uuid.New()
	r.db.Create(&account)
	if account.ID <= 0 {
		return account, fmt.Errorf("%s", "Cannot create account")
	}

	return account, nil
}

// Update saves the changes to an account in the database.
func (r accountRepository) Update(ctx context.Context, account entities.Account) error {
	return nil
}

// Delete deletes an account with the specified ID from the database.
func (r accountRepository) Delete(ctx context.Context, id string) error {
	return nil
}

// Count returns the number of the account records in the database.
func (r accountRepository) Count(ctx context.Context) (int, error) {
	return 10, nil
}

// Query retrieves the account records with the specified offset and limit from the database.
func (r accountRepository) Query(ctx context.Context, offset, limit int) ([]entities.Account, error) {
	var accounts []entities.Account

	return accounts, nil
}
