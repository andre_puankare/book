package entities

import (
	"github.com/jinzhu/gorm"
)

// Account a struct to rep user account
type Account struct {
	gorm.Model
	UUID      string `json:"uuid"`
	Email     string `gorm:"unique;not null"`
	Phone     string `gorm:"type:varchar(20);unique;not null"`
	Username  string `gorm:"type:varchar(30)"`
	FirstName string `gorm:"type:varchar(30)"`
	LastName  string `gorm:"type:varchar(30)"`
	Bio       string `gorm:"type:varchar(70)"`
	IsAdmin   bool   `gorm:"type:bool;default false;not null"`
	IsActive  bool   `gorm:"type:bool;default true;not null"`
	Password  string `json:"password"`
}
