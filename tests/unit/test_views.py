from unittest.mock import ANY
import pretend

from book import views as _views
from book.views import (
    echo,
    root,
    health,
    open_api_view,
)


def test_echo_no_body():
    assert echo(pretend.stub(
        body=b'',
        headers={'User-Agent': 'test'},
        method='GET',
        url='http://localhost:8080/_echo/',
    )) == {
        'headers': {'User-Agent': 'test'},
        'method': 'GET',
        'url': 'http://localhost:8080/_echo/',
    }


def test_echo_utf8_body():
    assert echo(pretend.stub(
        body=b'test',
        headers={'User-Agent': 'test'},
        method='GET',
        url='http://localhost:8080/_echo/',
    )) == {
        'body': 'test',
        'headers': {'User-Agent': 'test'},
        'method': 'GET',
        'url': 'http://localhost:8080/_echo/',
    }


def test_echo_binary_body():
    assert echo(pretend.stub(
        body=b'\xfftest',
        headers={'User-Agent': 'test'},
        method='GET',
        url='http://localhost:8080/_echo/',
    )) == {
        'body_b64': '/3Rlc3Q=',
        'headers': {'User-Agent': 'test'},
        'method': 'GET',
        'url': 'http://localhost:8080/_echo/',
    }


def test_root():
    assert root(ANY) == 'OK'


def test_health():
    request = pretend.stub(
        db=pretend.stub(
            execute=pretend.call_recorder(lambda sql: None),
        )
    )

    assert health(request) == 'OK'
    assert request.db.execute.calls == [pretend.call('SELECT 1')]


def test_open_api_view(monkeypatch):
    api = pretend.stub()
    services = pretend.stub()

    get_services = pretend.call_recorder(lambda: services)
    generate = pretend.call_recorder(lambda title, version: api)

    cornice_swagger = pretend.stub(generate=generate)
    cornice_swagger_cls = pretend.call_recorder(lambda s: cornice_swagger)

    monkeypatch.setattr(_views, 'CorniceSwagger', cornice_swagger_cls)
    monkeypatch.setattr(_views, 'get_services', get_services)

    assert open_api_view(None) is api
    assert get_services.calls == [pretend.call()]
    assert cornice_swagger_cls.calls == [pretend.call(services)]
    assert generate.calls == [pretend.call(title='BookAPI', version='1.0.0')]
