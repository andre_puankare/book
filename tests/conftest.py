from pytest import fixture

import alembic
import pyramid

from pyramid import config
from cornice.errors import Errors as CorniceErrors

from book.db import Session, metadata


class _Services:
    def __init__(self):
        self._services = {}

    def register_service(self, iface, context, service_obj, name=''):
        self._services[(iface, context, name)] = service_obj

    def find_service(self, iface, context=None, name=''):
        return self._services[(iface, context, name)]


class _FeatureFlags:
    def __getattr__(self, name):
        return False


@fixture
def pyramid_request(pyramid_services):
    dummy_request = pyramid.testing.DummyRequest()
    dummy_request.client = ''
    dummy_request.user_agent = 'pytest'
    dummy_request.remote_addr = '127.0.0.1'
    dummy_request.find_service = pyramid_services.find_service
    dummy_request.feature = _FeatureFlags()

    return dummy_request


@fixture
def cornice_request(pyramid_request):
    pyramid_request.errors = CorniceErrors()
    pyramid_request.validated = {}
    pyramid_request.info = {}
    pyramid_request.matchdict = {}

    return pyramid_request


@fixture(scope='session')
def app_config():
    settings = {
        'jwt.secret': 'test',
    }

    cfg = config.configure(settings=settings)

    alembic.command.upgrade(cfg.alembic_config(), 'head')

    return cfg


@fixture
def pyramid_services():
    return _Services()


@fixture
def db(app_config):
    engine = app_config.registry['sqlalchemy.engine']
    session = Session(bind=engine)

    try:
        yield session
    finally:
        session.rollback()

    for table in reversed(metadata.sorted_tables):
        session.execute(table.delete())

    session.commit()
    session.close()
