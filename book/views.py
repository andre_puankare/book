from base64 import b64encode

import logging

from pyramid_rpc.jsonrpc import jsonrpc_method
from pyramid.view import view_config

from cornice.service import get_services
from book.utils.cornice_swagger import CorniceSwagger


log = logging.getLogger(__name__)


@view_config(route_name='root', renderer='string')
def root(request):
    return 'OK'


@view_config(route_name='health', renderer='string')
def health(request):
    request.db.execute('SELECT 1')
    return 'OK'


@view_config(route_name='echo', renderer='json')
def echo(request):
    body = request.body
    out = {
        'headers': dict(request.headers),
        'method': request.method,
        'url': request.url,
    }

    if body:
        try:
            out['body'] = body.decode('utf-8')
        except UnicodeDecodeError:
            out['body_b64'] = b64encode(body).decode('ascii')

    return out


@view_config(
    route_name='cornice_swagger.open_api_path', renderer='json',
)
def open_api_view(request):
    return CorniceSwagger(
        get_services(),
    ).generate(
        title='BookAPI',
        version='1.0.0',
    )
