from book.accounts.events import UserEvent


class AuthEvent(UserEvent):
    pass


class UserLoggedIn(AuthEvent):
    pass


class UserRegistered(AuthEvent):
    pass


class UserRenewedAuthCredentials(AuthEvent):
    pass
