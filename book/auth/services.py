from binascii import hexlify
from calendar import timegm
from datetime import datetime
from functools import partial
from os import urandom

from zope.interface import implementer

from book.jwtbook import (
    InvalidJWTTokenError, decode_auth_token,
    decode_refresh_token, encode_token_pair,
)
from book.pyjwt import pyjwt_decode, pyjwt_encode

from .interfaces import (
    ILoginService,
    InvalidLoginSessionError,
    InvalidTokenError,
)
from .models import Login


def random_string(length=64):
    assert length > 0
    assert length % 2 == 0

    return hexlify(urandom(length // 2)).decode('ascii')


def _now():
    now = datetime.utcnow()

    return now, timegm(now.timetuple())


@implementer(ILoginService)
class DatabaseLoginService:
    def __init__(
            self, db, secret,
            auth_token_ttl=15 * 60,
            refresh_token_ttl=2 * 30 * 24 * 60 * 60,
            refresh_token_nbf=10 * 60,
            jwt_encode=pyjwt_encode,
            jwt_decode=pyjwt_decode,
    ):
        self.db = db
        self._pair = partial(
            encode_token_pair,
            secret,
            auth_token_ttl,
            refresh_token_ttl,
            refresh_token_nbf,
            encode=jwt_encode,
        )
        self._parse_auth = partial(decode_auth_token, secret, decode=jwt_decode)
        self._parse_refresh = partial(
            decode_refresh_token, secret, decode=jwt_decode,
        )

    @classmethod
    def create_service(cls, context, request):
        settings = request.registry.settings

        return cls(
            request.db, settings['jwt.secret'],
            auth_token_ttl=settings['jwt.auth.ttl'],
            refresh_token_ttl=settings['jwt.refresh.ttl'],
            refresh_token_nbf=settings['jwt.refresh.nbf'],
        )

    def _get(self, login_id):
        return self.db.query(Login).get(login_id)

    def login(self, user_id, *, device, ip, location):
        refresh_token = random_string()
        login = Login(
            user_id=user_id,
            refresh_token=refresh_token,
            device=device,
            ip=ip,
            location=location,
        )

        self.db.add(login)
        self.db.flush()

        _, now = _now()

        return self._pair(now, login.id, user_id, refresh_token)

    def refresh(self, refresh_token, *, device, ip, location):
        try:
            subject, login_id = self._parse_refresh(refresh_token)
        except InvalidJWTTokenError as e:
            raise InvalidTokenError from e

        login = self._get(login_id)

        if login is None:
            raise InvalidLoginSessionError('Login session does not exists')

        if not (login.is_valid and login.refresh_token):
            raise InvalidLoginSessionError('Login session expired')

        if login.refresh_token != subject:
            raise InvalidTokenError('Refresh token is no longer valid')

        dtnow, now = _now()
        refresh_token = random_string()

        login.last_activity = dtnow
        login.refresh_token = refresh_token
        login.device = device
        login.ip = ip
        login.location = location

        self.db.flush()

        access_token, refresh_token = self._pair(
            now, login.id, login.user_id, refresh_token,
        )

        return access_token, refresh_token, login.user_id

    def check(self, auth_token):
        try:
            return self._parse_auth(auth_token)[0]
        except InvalidJWTTokenError:
            return None

    def logout(self, refresh_token):
        try:
            self.force_logout(self._parse_refresh(refresh_token)[1])
        except InvalidJWTTokenError as e:
            raise InvalidTokenError from e

    def logout_all_except_current(self, auth_token):
        try:
            login = self._get(self._parse_auth(auth_token)[1])
        except InvalidJWTTokenError as e:
            raise InvalidTokenError from e

        self.db.query(Login).filter(
            Login.user_id == login.user_id,
            Login.is_valid,
            Login.id != login.id,
        ).update({
            'is_valid': False,
            'refresh_token': '',
        }, synchronize_session=False)
        self.db.flush()

    def force_logout(self, login_id):
        login = self._get(login_id)
        login.is_valid = False
        login.refresh_token = ''

        self.db.flush()

    def list_active(self, user_id):
        return self.db.query(Login).filter(
            Login.user_id == user_id,
            Login.is_valid,
        ).order_by(
            Login.last_activity.desc(),
        ).all()
