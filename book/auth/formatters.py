def format_registered_user(user, email, phone):
    return {
        'id': user.id,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'username': user.username,
        'email':  email,
        'phone': phone,
        'password': '',
    }
