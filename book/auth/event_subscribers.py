from time import sleep

from pyramid.events import subscriber

from .events import (
    UserRegistered,
    AuthEvent,
    UserLoggedIn,
    UserRenewedAuthCredentials,
)


@subscriber(UserRegistered)
def notify_owner(event):
    print('user registered', event.user)

