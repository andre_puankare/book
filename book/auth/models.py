from sqlalchemy.orm import relationship
from sqlalchemy import (
    Column,
    Boolean,
    Integer,
    DateTime,
    String,
    ForeignKey,
    sql,
)


from book import db
from book.utils.attrs import make_repr


class Login(db.Model):
    """Represents single login session."""

    __tablename__ = 'logins'
    __repr__ = make_repr("user_id")

    user_id = Column(
        Integer,
        ForeignKey('users.id'),
        nullable=False,
        index=True,
        doc='Book user',
    )
    is_valid = Column(
        Boolean,
        default=True,
        nullable=False,
        doc='Validity of the login session',
    )
    date = Column(
        DateTime,
        nullable=False,
        server_default=sql.func.now(),
        doc='Time when user got logged in',
    )
    last_activity = Column(
        DateTime,
        nullable=False,
        server_default=sql.func.now(),
        doc='Time of the last auth token refresh',
    )
    refresh_token = Column(
        String(length=128),
        default='',
        nullable=False,
        doc='Refresh token used for reissuing JWT',
    )
    device = Column(
        String(length=128),
        default='',
        nullable=False,
        doc=(
            'Name of the device user logged in. E.g. Firefox, Teamicate '
            'Android App, etc..'
        ),
    )
    ip = Column(
        String(length=128),
        default='',
        nullable=False,
        doc='IP address',
    )
    location = Column(
        String(length=128),
        default='',
        nullable=False,
        doc='Country and City',
    )

    user = relationship('User', backref='logins')
