from pyramid.security import NO_PERMISSION_REQUIRED

from cornice.validators import (
    colander_body_validator,
)
from cornice.resource import resource, view

from book.accounts.interfaces import IUserService
from book.accounts.validators import (
    username_is_available,
    phone_is_available,
    email_is_available,
)
from book.utils.cornice import BaseResource, ok_response

from .formatters import format_registered_user
from .schemas import (
    UserRegistrationSchema,
    UserRegisteredSchema,
    PasswordLoginSchema,
    AuthTokensSchema,
    RefreshTokenSchema,
)
from .events import UserRegistered

from .interfaces import ILoginService
from .interfaces import (
    InvalidLoginSessionError,
    InvalidTokenError,
)


def slice_user_agent(user_agent, max_length=128):
    if len(user_agent) < max_length:
        return user_agent

    return user_agent[:max_length - 3] + '...'


@resource(
    path='/api/auth/register/password',
    tags=['auth'],
    traverse='/',
)
class AuthRegistrationView(BaseResource):
    RESPONSE_SCHEMA = UserRegisteredSchema()

    @view(
        permission=NO_PERMISSION_REQUIRED,
        schema=UserRegistrationSchema,
        validators=(
                colander_body_validator,
                username_is_available,
                phone_is_available,
                email_is_available,
        ),
    )
    def post(self):
        """Register new user with phone and email."""

        request = self.request
        usr_svc = request.find_service(IUserService)
        validated = request.validated

        user = usr_svc.create_user(
            username=validated['username'],
            first_name=validated['first_name'],
            last_name=validated['last_name'],
            password=validated['password'],
        )
        usr_svc.add_email(user.id, validated['email'])
        usr_svc.add_phone(user.id, validated['phone'])

        event = UserRegistered(user, request, user.id)
        request.registry.notify(event)

        return self.serialize(
            format_registered_user(
                user,
                validated['email'],
                validated['phone'],
            ))


@resource(
    path='api/auth/login/password',
    tags=['auth'],
    traverse='/',
)
class AuthLoginPasswordView(BaseResource):
    RESPONSE_SCHEMA = AuthTokensSchema()

    @view(
        permission=NO_PERMISSION_REQUIRED,
        schema=PasswordLoginSchema,
        validators=(colander_body_validator, )
    )
    def post(self):
        """Login with password."""
        request = self.request
        validated = request.validated
        username, password = validated['username'], validated['password']
        usr_svc = request.find_service(IUserService)

        user = usr_svc.get_user_by_username(username)
        if user is None:
            request.errors.add('body', 'username', 'user not found')
            request.errors.status = 404
            return

        authorized = usr_svc.check_password(user.id, password)
        if authorized is False:
            request.errors.add('body', 'password', 'wrong password')
            request.errors.status = 401
            return

        login_service = request.find_service(ILoginService)
        access_token, refresh_token = login_service.login(
            user.id,
            device=slice_user_agent(request.user_agent),
            ip=request.remote_addr,
            location='',
        )

        return self.serialize({
            'access_token': access_token,
            'refresh_token': refresh_token,
            'registered': True,
        })


@resource(
    path='api/auth/refresh',
    tags=['auth'],
    factory=None,
    traverse='/',
)
class TokenRefreshView(BaseResource):
    RESPONSE_SCHEMA = AuthTokensSchema()

    @view(
        permission=NO_PERMISSION_REQUIRED,
        schema=RefreshTokenSchema(),
        validators=(colander_body_validator, ),
    )
    def post(self):
        """Refresh access token."""
        # TODO: use pyramid_useragent

        login_service = self.request.find_service(ILoginService)

        try:
            access_token, refresh_token, user_id = login_service.refresh(
                self.request.validated['refresh_token'],
                device=slice_user_agent(self.request.user_agent),
                ip=self.request.remote_addr,
                location='',
            )
        except InvalidLoginSessionError:
            self.request.errors.add('body', 'refresh_token', 'expired')
            self.request.errors.status = 400
            return
        except InvalidTokenError:
            self.request.errors.add('body', 'refresh_token', 'invalid')
            self.request.errors.status = 400
            return

        return self.serialize({
            'access_token': access_token,
            'refresh_token': refresh_token,
            'registered': True,
        })


@resource(
    path='api/auth/logout/password',
    tags=['auth'],
    factory=None,
    traverse='/',
)
class TokenLogoutView(BaseResource):
    @ok_response
    @view(
        permission=NO_PERMISSION_REQUIRED,
        schema=RefreshTokenSchema(),
        validators=(colander_body_validator, ),
    )
    def post(self):
        """Delete access token (logout)."""

        login_service = self.request.find_service(ILoginService)

        try:
            login_service.logout(self.request.validated['refresh_token'])
        except InvalidLoginSessionError:
            self.request.errors.add('body', 'refresh_token', 'expired')
            self.request.errors.status = 400
            return
        except InvalidTokenError:
            self.request.errors.add('body', 'refresh_token', 'invalid')
            self.request.errors.status = 400
            return
        except KeyError:
            self.request.errors.add('body', 'refresh_token', 'not provided')
            self.request.errors.status = 400
            return

        return self.ok('Successfully logged out.')
