from typing import Tuple

from zope.interface import Interface


class LoginServiceError(Exception):
    pass


class InvalidTokenError(LoginServiceError):
    pass


class InvalidLoginSessionError(LoginServiceError):
    pass


class ILoginService(Interface):
    def login(user_id, *, device, ip, location) -> Tuple[str, str]:
        """Issue new authorization token.

        Returns:
            Tuple of auth token and refresh token.
        """

    def refresh(refresh_token, *, device, ip, location) -> Tuple[str, str, int]:
        """Make new token pair using refresh_token.

        Returns:
            Tuple of access_token, refresh_token and user_id
        """

    def check(auth_token):
        """Check if auth token is still valid.

        Returns:
            User id or None
        """

    def logout(refresh_token):
        """Invalidate login session."""

    def logout_all_except_current(auth_token):
        """Invalidate all login session except current."""

    def force_logout(login_id):
        """Force invalidate login session"""

    def list_active(user_id):
        """Return list of active logins."""


class CipherServiceError(Exception):
    pass


class ICipherService(Interface):
    def encode(byte_string: bytes):
        """Enciphers a byte string

        Args:
            byte_string: byte string that needs to be enciphered

        Returns:
            ByteString: enciphered byte string
        """

    def decode(byte_string: bytes):
        """Enciphers a byte string

        Args:
            byte_string: byte string that needs to be deciphered

        Returns:
            ByteString: deciphered byte string
        """



