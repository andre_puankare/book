import colander

from colander_tools.null import NullType
from colander_tools.strict import Boolean, String


from colander import (
    All,
    DateTime,
    Length,
    MappingSchema,
    SchemaNode,
    SequenceSchema,
    String,
    Boolean,
    Integer,
    Range,
    drop,
    required,
)

from book.accounts.schemas import UserResponse
from book.utils import remove_multiple_spaces, strip_whitespace
from book.utils.colander import (
    Any,
    email,
    is_null,
    password,
    phone_number,
    strong_password,
    username,
)


class UserRegistrationSchema(MappingSchema):
    first_name = SchemaNode(
        String(),
        preparer=[remove_multiple_spaces, strip_whitespace],
        validator=Length(max=200),
        missing=required,
        example='John',
    )
    last_name = SchemaNode(
        String(),
        preparer=[remove_multiple_spaces, strip_whitespace],
        validator=Length(max=200),
        missing=required,
        example='Doe',
    )
    username = SchemaNode(
        NullType(String()),
        validator=Any(is_null, username),
        missing=drop,
        example='admin',
    )
    password = SchemaNode(
        String(),
        validator=All(password, strong_password),
        missing=required,
        example='hackme',
    )
    email = SchemaNode(
        String(),
        validator=email,
        missing=required,
    )
    phone = SchemaNode(
        String(),
        validator=phone_number,
        missing=required,
        example='+19995550100',
    )


class UserRegisteredSchema(UserRegistrationSchema):
    id = SchemaNode(
        Integer(),
        missing=drop,
        default=drop,
    )
    email = SchemaNode(
        String(),
        missing=drop,
        default=drop,
    )
    phone = SchemaNode(
        String(),
        missing=drop,
        default=drop,
    )


class PasswordLoginSchema(MappingSchema):
    username = SchemaNode(
        String(),
        missing=required,
        missing_msg='Email, phone number or username is required',
    )
    password = SchemaNode(String(), missing=required)


class AuthTokensSchema(MappingSchema):
    access_token = SchemaNode(
        String(),
        default=drop,
        missing=drop,
    )
    refresh_token = SchemaNode(
        String(),
        default=drop,
        missing=required,
    )
    user = UserResponse(default=drop, missing=drop)
    registered = SchemaNode(
        Boolean(),
        default=False,
        missing=drop,
    )


class RefreshTokenSchema(MappingSchema):
    access_token = SchemaNode(
        String(),
        default=drop,
        missing=drop,
    )
    refresh_token = SchemaNode(
        String(),
        default=drop,
        missing=required,
    )
