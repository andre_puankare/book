from pyramid.authorization import ACLAuthorizationPolicy
from pyramid_multiauth import MultiAuthenticationPolicy


from .interfaces import ILoginService
from .n_policies import (
    BasicAuthAuthenticationPolicy,
    JWTAuthenticationPolicy,
)
from .services import DatabaseLoginService
from .views import AuthRegistrationView, AuthLoginPasswordView


def includeme(config):
    config.register_service_factory(
        DatabaseLoginService.create_service,
        ILoginService,
    )

    config.add_cornice_resource(AuthRegistrationView)
    config.add_cornice_resource(AuthLoginPasswordView)

    config.set_authentication_policy(
        MultiAuthenticationPolicy([
            JWTAuthenticationPolicy(),
            BasicAuthAuthenticationPolicy(),
        ])
    )
    config.set_authorization_policy(ACLAuthorizationPolicy())
