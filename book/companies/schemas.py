from colander import (
    SchemaNode,
    MappingSchema,
    String,
    drop,
    required,
    Length,
    Integer,
)


class CompanySchema(MappingSchema):
    user_id = SchemaNode(
        Integer(),
        missing=required,
        example='1',
    )
    category_id = SchemaNode(
        Integer(),
        missing=required,
        example='1',
    )
    description = SchemaNode(
        String(),
        missing=required,
        validator=Length(max=512),
        example='Microsoft CO',
    )
    name = SchemaNode(
        String(),
        missing=required,
        validator=Length(max=96),
        example='Microsoft',
    )


class CategorySchema(MappingSchema):
    name = SchemaNode(
        String(),
        missing=required,
        validator=Length(max=96),
        example='Cat',
    )
    description = SchemaNode(
        String(),
        missing=required,
        validator=Length(max=512),
        example='Desc',
    )
