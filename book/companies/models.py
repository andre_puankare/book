from sqlalchemy import (
    Integer,
    DateTime,
    String,
    Text
)
from sqlalchemy import (
    Column,
    ForeignKey,
    sql,
)
from sqlalchemy.orm import relationship


from book.db import Model
from book.utils.attrs import make_repr


class Company(Model):
    __tablename__ = 'companies'

    __repr__ = make_repr('name')

    user_id = Column(
        Integer,
        ForeignKey('users.id'),
        nullable=False,
        index=True,
    )
    category_id = Column(
        Integer,
        ForeignKey('categories.id'),
        nullable=False,
        index=True,
    )
    name = Column(
        String(length=256),
        nullable=False,
        unique=True,
        index=True,
    )
    description = Column(Text, nullable=False)
    created = Column(
        DateTime,
        nullable=False,
        server_default=sql.func.now(),
        doc='Time when Company created',
    )

    user = relationship('User', backref='companies')
    category = relationship('Company', backref='categories')


class Category(Model):
    __tablename__ = 'categories'

    __repr__ = make_repr('name')

    name = Column(
        String(length=256),
        nullable=False,
        unique=True,
        index=True,
    )
    description = Column(Text, nullable=False)
