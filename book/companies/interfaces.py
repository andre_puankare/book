from zope.interface import Attribute, Interface


class CompanyServiceError(Exception):
    pass


class CompanyAlreadyExistsError(CompanyServiceError):
    pass


class ICompanyService(Interface):
    def create(
            user_id: int,
            category_id: int,
            name: str,
            description: str,
    ):
        """
        Creates new user company.
        """

    def delete_by_id(id: int):
        """
        Delete company by id
        """
