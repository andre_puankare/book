import click

from traitlets.config.loader import Config
from IPython import start_ipython


from book.cli import book
from book.db import Session

import re
import sys
from datetime import date, datetime, time
from enum import Enum
from inspect import isclass

from zope.interface.interface import InterfaceClass

from book.db import ModelBase


def discover_interfaces(modules):
    for module_name, module in modules.items():
        if not re.match(r'book\..*\.interfaces', module_name):
            continue

        for name in dir(module):
            value = getattr(module, name)

            if isinstance(value, InterfaceClass):
                yield name, value

            if isclass(value):
                if issubclass(value, Enum):
                    yield name, value


def discover_models(modules, class_or_tuple):
    for module_name, module in modules.items():
        if not re.match(r'book\..*\.models', module_name):
            continue

        for name in dir(module):
            value = getattr(module, name)

            if not isclass(value):
                continue

            if issubclass(value, class_or_tuple):
                yield name, value


def ipython(**locals_):
    start_ipython(
        argv=[],
        user_ns=locals_,
        config=Config(
            HistoryManager={
                'enabled': True,
            },
            TerminalInteractiveShell={
                'confirm_exit': False,
                'term_title': False,
            },
            TerminalIPythonApp={
                'display_banner': False,
            },
        ),
    )


@book.command()
@click.pass_obj
def shell(config):
    """
    Open up a Python shell with Book prec. in it.
    """
    session = Session(bind=config.registry['sqlalchemy.engine'])
    ipython(
        db=session,
        date=date,
        datetime=datetime,
        time=time,
        config=config,
        **dict(discover_models(sys.modules, ModelBase)),
        **dict(discover_interfaces(sys.modules)),
    )
