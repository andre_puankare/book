from importlib import import_module

import click

try:
    import hupper
except ImportError:  # pragma: no cover
    hupper = None

from waitress import serve_paste

from book.cli import book


@book.command()
@click.option(
    '--listen',
    'listen',
    default='localhost:8080',
    help='Tell waitress to listen on an ip port combination.',
)
@click.option(
    '--reload/--no-reload',
    'reload_',
    default=True,
    help='Enable source reload using hupper.',
)
@click.option(
    '--module',
    'module',
    default='book.wsgi',
    hidden=True,
)
@click.pass_obj
def serve(config, listen, reload_, module):
    """Starts a lightweight development Web server on the local machine."""

    if reload_:
        if hupper is None:
            raise click.ClickException('Source reload is not supported.')

        hupper.start_reloader('book.cli.book')

    wsgi = import_module(module)

    serve_paste(wsgi.application, config, listen=listen)
