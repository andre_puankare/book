import click
import alembic.command

from book.cli.db import db


@db.command()
@click.argument("revision", required=True)
@click.pass_obj
def downgrade(config, revision, **kwargs):
    """
    Downgrade database.
    """
    engine = config.registry['sqlalchemy.engine']
    with engine.begin() as connection:
        alembic_config = config.alembic_config()
        alembic_config.attributes['connection'] = connection
        alembic.command.downgrade(alembic_config, revision, **kwargs)
