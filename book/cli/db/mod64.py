from datetime import datetime

import click

from book.cli.db import db
from book.db import Session
from book.accounts.models import User


@db.command()
@click.pass_obj
def mod64(config, **kwargs):
    """
    Mod 64.
    """
    session = Session(bind=config.registry['sqlalchemy.engine'])
    session.add(User(
        id=1,
        username='god',
        first_name='god',
        last_name='god',
        password='',
        is_admin=True,
        is_active=True,
        date_joined=datetime.fromtimestamp(0),
        date_deactivated=datetime.fromtimestamp(0),
    ))
    session.commit()
