from book.cli import book


@book.group()
def db():
    """
    Manage the Book Database.
    """
