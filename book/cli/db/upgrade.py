import alembic.command
import click

from book.cli.db import db


@db.command()
@click.argument("revision", required=True)
@click.option('--sql', default=False, is_flag=True)
@click.pass_obj
def upgrade(config, revision, **kwargs):
    """
    Upgrade database.
    """
    engine = config.registry['sqlalchemy.engine']
    with engine.begin() as connection:
        alembic_config = config.alembic_config()
        alembic_config.attributes['connection'] = connection
        alembic.command.upgrade(alembic_config, revision, **kwargs)
