import sys

import hupper

from waitress import serve

from book.config import configure


application = configure().make_wsgi_app()


def main(args=sys.argv[1:]):
    if '--reload' in args:
        # start_reloader will only return in a monitored subprocess
        reloader = hupper.start_reloader('book.wsgi.main')

        # monitor an extra file
        reloader.watch_files(['.'])

    serve(application, host='0.0.0.0', port=8000)
