from sqlalchemy import (
    Integer,
    DateTime,
    String,
    Text
)
from sqlalchemy import (
    Column,
    ForeignKey,
    sql,
    Boolean,
)
from sqlalchemy.orm import relationship


from book.db import Model
from book.utils.attrs import make_repr


class Profile(Model):
    __tablename__ = 'profiles'

    __repr__ = make_repr('name')

    user_id = Column(
        Integer,
        ForeignKey('users.id'),
        nullable=False,
        index=True,
    )
    company_id = Column(
        Integer,
        ForeignKey('companies.id'),
        nullable=False,
        index=True,
    )
    info = Column(Text, nullable=False)
    created = Column(
        DateTime,
        nullable=False,
        server_default=sql.func.now(),
        doc='Time when Company created',
    )
    verified = Column(
        Boolean,
        default=False,
        nullable=False,
    )

    user = relationship("User", back_populates="profile")
    company = relationship("Company", back_populates="profiles")
