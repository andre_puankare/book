import colander

from colander_tools.null import NullType
from colander_tools.strict import (
    Boolean,
    Float,
    Integer,
    Mapping,
    String,
)

from cornice.validators import colander_querystring_validator

from cornice_swagger.converters.exceptions import NoSuchConverter
from cornice_swagger.converters.schema import (
    ArrayTypeConverter,
    BooleanTypeConverter,
    DateTimeTypeConverter,
    DateTypeConverter,
    IntegerTypeConverter,
    NumberTypeConverter,
    ObjectTypeConverter,
    StringTypeConverter,
    TimeTypeConverter,
    TypeConverter,
)
from cornice_swagger.swagger import (
    CorniceSwagger as _CorniceSwagger,
    ParameterHandler as _ParameterHandler,
)
from cornice_swagger.util import body_schema_transformer

from .colander import (
    InfFloat,
    NaiveDateTime,
    NullDate,
    NullDateTime,
    NullString,
    Number,
)


class EnumTypeConverter(TypeConverter):
    type = 'string'

    def convert_type(self, schema_node):
        converted = super().convert_type(schema_node)

        attr = schema_node.typ.attr or 'name'

        converted['enum'] = [
            getattr(e, attr)
            for e in schema_node.typ.enum_cls
        ]

        return converted


class NullTypeConverter(TypeConverter):
    def convert_type(self, schema_node):
        typ = schema_node.typ.typ
        schema_node = schema_node.clone()
        schema_node.typ = typ

        return self.dispatcher(schema_node)


CONVERTERS = {
    colander.Boolean: BooleanTypeConverter,
    colander.Date: DateTypeConverter,
    colander.DateTime: DateTimeTypeConverter,
    colander.Float: NumberTypeConverter,
    colander.Integer: IntegerTypeConverter,
    colander.Mapping: ObjectTypeConverter,
    colander.Sequence: ArrayTypeConverter,
    colander.String: StringTypeConverter,
    colander.Time: TimeTypeConverter,
    colander.Enum: EnumTypeConverter,
    Boolean: BooleanTypeConverter,
    Float: NumberTypeConverter,
    InfFloat: NumberTypeConverter,
    Number: NumberTypeConverter,
    Integer: IntegerTypeConverter,
    String: StringTypeConverter,
    Mapping: ObjectTypeConverter,
    NaiveDateTime: DateTimeTypeConverter,
    NullDate: DateTypeConverter,
    NullDateTime: DateTimeTypeConverter,
    NullString: StringTypeConverter,
    NullType: NullTypeConverter,
}


class TypeConversionDispatcher:
    def __init__(self, custom_converters=None, default_converter=None):
        self.converters = dict(CONVERTERS)

        if custom_converters:
            self.converters.update(custom_converters)

        self.default_converter = default_converter

    def __call__(self, schema_node):
        schema_type = schema_node.typ

        try:
            converter_cls = self.converters[schema_type]
        except KeyError:
            schema_type = type(schema_type)

            try:
                converter_cls = self.converters[schema_type]
            except KeyError:
                if self.default_converter:
                    converter_cls = self.default_converter
                else:
                    raise NoSuchConverter

        converter = converter_cls(self)
        converted = converter(schema_node)

        converted.pop('default', None)

        return converted


def querystring_schema_transformer(schema, args):
    validators = args.get('validators', [])
    if colander_querystring_validator in validators:
        querystring_schema = schema
        schema = colander.MappingSchema()
        schema['querystring'] = querystring_schema
    return schema


class ParameterHandler(_ParameterHandler):
    def from_path(self, path):  # pragma: no cover
        # XXX: cornice swagger does not recognise parameters like test/{a}-{b}
        return super().from_path(path.replace('-', '/'))


class CorniceSwagger(_CorniceSwagger):
    type_converter = TypeConversionDispatcher
    parameters = ParameterHandler
    summary_docstrings = True
    schema_transformers = [
        body_schema_transformer,
        querystring_schema_transformer,
    ]

    def _build_paths(self):  # pragma: no cover
        # XXX: paths without leading / are invalid according to swagger spec
        paths, tags = super()._build_paths()

        return {
            path if path.startswith('/') else '/' + path: path_obj
            for path, path_obj in paths.items()
        }, tags
