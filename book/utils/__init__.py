from re import sub


def strip_whitespace(s: str) -> str:
    return s.strip() if s else s


def remove_multiple_spaces(s: str) -> str:
    return sub(' +', ' ', s) if s else s
