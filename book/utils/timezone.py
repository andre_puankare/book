from datetime import datetime


from pytz import utc


def is_naive(dt: datetime) -> bool:
    return dt.tzinfo is None or dt.tzinfo.utcoffset(dt) is None


def to_utc(dt: datetime) -> datetime:
    if is_naive(dt):
        return dt
    return dt.astimezone(utc).replace(tzinfo=None)
