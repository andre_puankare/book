import operator
from functools import lru_cache
from math import inf

from colander import (
    Date,
    DateTime,
    Email,
    Float,
    Invalid,
    Length,
    MappingSchema,
    Regex,
    SchemaNode,
    SchemaType,
    _,
    deferred,
    drop,
    null,
)

from colander_tools.strict import Integer, String

from dateutil.rrule import rrulestr

from pytz import timezone as pytz_timezone
from pytz.exceptions import UnknownTimeZoneError

from .timezone import to_utc


class StatusMessageSchema(MappingSchema):
    message = SchemaNode(String(), default=drop, example='Success message.')
    status = SchemaNode(String(), default=drop, example='ok')


class Regex(Regex):
    def __deepcopy__(self, memodict=None):
        return self


email = Email(msg='Invalid email address')
phone_number = Regex(r'^\+[1-9]\d{1,14}$', msg='Invalid phone number')
username = Regex(r'^[a-zA-Z0-9._-]{3,42}$', msg='Invalid username')
password = Length(
    min=8, max=128,
    min_err='Password is too short',
    max_err='Password is too long',
)
strong_password = Regex(
    r'(?=.*[a-z]{1,})(?=.*[0-9]{1,})(?=.*[A-Z]{1,})',
    msg='Must contain letters of different case and numeric values.',
)
rgb_hex = Regex(
    r'^#[a-fA-F0-9]{6}$',
    msg='Must be RGB hex color.',
)


def is_null(node, value):
    if value is not None:
        raise Invalid(node, 'is not null')


def timezone(node, value):
    try:
        pytz_timezone(value)
    except UnknownTimeZoneError:
        raise Invalid(node, '%s is not a valid timezone name' % value)


def rrule(node, value):
    try:
        rrulestr(value)
    except ValueError:
        raise Invalid(node, '%s is not a valid RRULE string' % value)


@deferred
def objects(node, kw):
    return kw['subschema']


class PageSchema(MappingSchema):
    count = SchemaNode(Integer())
    page = SchemaNode(Integer())
    pages = SchemaNode(Integer())
    size = SchemaNode(Integer())
    results = objects


class ObjectResponseSchema(MappingSchema):
    body = objects


class Any:
    def __init__(self, *validators):
        self.validators = validators

    def __call__(self, node, value):
        excs = []

        for validator in self.validators:
            try:
                validator(node, value)
                return
            except Invalid as e:
                excs.append(e)

        if not excs:
            return

        exc = Invalid(node, [exc.msg for exc in excs])

        for e in excs:
            exc.children.extend(e.children)

        raise exc


class InfFloat(Float):
    def __init__(self, *args, neg=False, **kwargs):
        super().__init__(*args, **kwargs)

        self.neg = neg

    def deserialize(self, node, cstruct):
        ds = super().deserialize(node, cstruct)

        if ds is null:
            return null

        if ds < 0:
            if self.neg:
                return -inf
            return inf

        return ds


class NaiveDateTime(DateTime):
    def __init__(self):
        super().__init__(default_tzinfo=None, format=None)

    def serialize(self, node, appstruct):
        if appstruct is null:
            return drop

        serialized = super().serialize(node, appstruct)

        if serialized is null:
            return drop

        return serialized

    def deserialize(self, node, cstruct):
        dt = super().deserialize(node, cstruct)

        if dt is null:
            return dt

        return to_utc(dt)


class NullDate(Date):
    def serialize(self, node, appstruct):
        if appstruct is None:
            return drop
        return super().serialize(node, appstruct)


class NullDateTime(DateTime):
    def serialize(self, node, appstruct):
        if appstruct is None:
            return drop
        return super().serialize(node, appstruct)


class NullString(String):
    def serialize(self, node, appstruct):
        if appstruct is None:
            return drop
        return super().serialize(node, appstruct)


class XDate(MappingSchema):
    date = SchemaNode(NullDate(), default=drop, missing=null)
    datetime = SchemaNode(NaiveDateTime(), default=drop, missing=null)
    tz = SchemaNode(
        NullString(),
        default=drop,
        missing=null,
        validator=timezone,
    )


class Number(SchemaType):
    def serialize(self, node, appstruct):
        if appstruct is null:
            return null

        if isinstance(appstruct, (int, float)):
            return appstruct

        raise Invalid(
            node, _('"${val}" is not a number', mapping={'val': appstruct}),
        )

    def deserialize(self, node, cstruct):
        if cstruct is null:
            return null

        if isinstance(cstruct, (int, float)):
            return cstruct

        raise Invalid(
            node, _('"${val}" is not a number', mapping={'val': cstruct}),
        )


@lru_cache(maxsize=None)
def end_after_start(start_field='start', end_field='end', inclusive=False):
    if inclusive:
        op = operator.lt
        msg = f'{end_field} must be after or equal to {start_field}'
    else:
        op = operator.le
        msg = f'{end_field} must be after {start_field}'

    def validator(node, cstruct):
        try:
            end, start = cstruct[end_field], cstruct[start_field]
        except KeyError:
            return

        if op(end, start):
            raise Invalid(node, msg)

    return validator


@lru_cache(maxsize=None)
def has_start_or_end(start_field='start', end_field='end'):
    msg = f'at least one of {end_field} or {start_field} must be provided'

    def validator(node, cstruct):
        if not (end_field in cstruct or start_field in cstruct):
            raise Invalid(node, msg)

    return validator


class AnyType(SchemaType):
    def serialize(self, node, appstruct):
        return appstruct

    def deserialize(self, node, cstruct):
        return cstruct
