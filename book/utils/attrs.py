from sqlalchemy.orm.exc import DetachedInstanceError


def make_repr(*attrs, _self=None):
    def _repr(self=None):
        if self is None and _self is not None:
            self = _self

        try:
            return "{}({})".format(
                self.__class__.__name__,
                ", ".join("{}={}".format(a, repr(getattr(self, a))) for a in attrs),
            )
        except DetachedInstanceError:
            return "{}(<detached>)".format(self.__class__.__name__)

    return _repr

