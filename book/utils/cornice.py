from functools import wraps

from typing import Optional

from colander import SchemaNode

from typing import Any, Optional, Type, TypeVar

from pyramid.security import (
    Deny,
    Allow,
    ALL_PERMISSIONS,
    Authenticated,
)
from .colander import ObjectResponseSchema, PageSchema, StatusMessageSchema


def list_response(fn):
    fn.list = True
    return fn


def paged_response(fn):
    fn.paged = True
    return fn


def ok_response(fn):
    fn.ok = True
    return fn


def when_valid(validator, **kwargs):
    """Run validator only if there is no errors yet."""

    @wraps(validator)
    def wrapper(request, **kwargs):
        if len(request.errors) > 0:
            return
        validator(request, **kwargs)

    return wrapper


T = TypeVar('T')


class ServiceDescriptor:
    def __init__(self, iface: Type[T], is_static: bool = False):
        self.iface = iface
        self.is_static = is_static

    def __get__(self, obj: Any, cls: type = None) -> T:
        if obj is None:
            if self.is_static:
                obj = cls
            else:
                return None
        return obj.request.find_service(self.iface)


class ResourceMeta(type):
    def __init__(cls, *args, **kwargs):
        for attr in dir(cls):
            f = getattr(cls, attr)
            views = getattr(f, '__views__', [])
            if len(views) != 1:
                continue

            if getattr(f, 'ok', False):
                schema = StatusMessageSchema(description='OK')
            elif getattr(f, 'list', False):
                schema = cls.COLLECTION_PARAMS_SCHEMA
            else:
                schema = cls.RESPONSE_SCHEMA

            if not schema:
                continue

            if getattr(f, 'paged', False):
                schema = PageSchema().bind(subschema=schema)

            try:
                response_schemas = views[0]['response_schemas']
            except KeyError:
                views[0]['response_schemas'] = response_schemas = {}
            response_schemas[200] = ObjectResponseSchema(
                description='OK',
            ).bind(subschema=schema)
            response_schemas[400] = ObjectResponseSchema(
                description='Bad request.',
            )


class BaseResource(metaclass=ResourceMeta):
    RESPONSE_SCHEMA: Optional[SchemaNode] = None
    COLLECTION_RESPONSE_SCHEMA: Optional[SchemaNode] = None

    PARAMS_SCHEMA: Optional[SchemaNode] = None
    COLLECTION_PARAMS_SCHEMA: Optional[SchemaNode] = None

    __acl__ = [
        (Deny, 'group:inactive', ALL_PERMISSIONS),
        (Allow, 'group:admins', ALL_PERMISSIONS),
        (Allow, Authenticated, 'view'),
    ]

    def __init__(self, request, context=None):
        self.request = request
        self.context = context
        self._page_schema = None
        self._log = None

    def ok(self, message):
        return StatusMessageSchema().serialize({
            'message': message,
            'status': 'ok',
        })

    @property
    def page_schema(self):
        schema = self._page_schema

        if schema is None:
            assert self.COLLECTION_RESPONSE_SCHEMA
            self._page_schema = schema = PageSchema().bind(
                subschema=self.COLLECTION_RESPONSE_SCHEMA,
            )

        return schema

    @property
    def user_id(self):
        return str(self.request.authenticated_userid)

    def serialize(self, appstruct):
        assert self.RESPONSE_SCHEMA
        return self.RESPONSE_SCHEMA.serialize(appstruct)

    def collection_serialize(self, appstruct):
        assert self.COLLECTION_RESPONSE_SCHEMA
        return self.COLLECTION_RESPONSE_SCHEMA.serialize(appstruct)

    def page_serialize(self, appstruct, count, page, size):
        if size > 0:
            pages = (count + size - 1) // size
        else:
            pages = -1

        return self.page_schema.serialize({
            'count': count,
            'page': page,
            'pages': pages,
            'size': size,
            'results': appstruct,
        })


