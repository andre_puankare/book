from functools import partial


class InvalidJWTTokenError(Exception):
    pass


AUTH = 'auth'
REFRESH = 'refresh'
EMAIL = 'email'


def encode_auth_token(
        secret, ttl, now, login_id, user_id, *,
        encode, **kwargs,
):
    assert encode is not None
    return encode({
        'aud': [AUTH],
        'exp': now + ttl,
        'iat': now,
        'lid': login_id,
        'sub': user_id,
    }, secret, **kwargs)


def encode_refresh_token(
        secret, ttl, nbf, now, login_id, refresh_token, *,
        encode, **kwargs,
):
    assert encode is not None
    return encode({
        'aud': [REFRESH],
        'exp': now + ttl,
        'iat': now,
        'lid': login_id,
        'nbf': now + nbf,
        'sub': refresh_token,
    }, secret, **kwargs)


def encode_email_confirmation_token(
        secret, ttl, now, email, user_id, *,
        encode, **kwargs,
):
    assert encode is not None
    return encode({
        'aud': [EMAIL],
        'exp': now + ttl,
        'iat': now,
        'sub': email,
        'uid': user_id,
    }, secret, **kwargs)


def encode_token_pair(
        secret, auth_ttl, refresh_ttl, refresh_nbf, now,
        login_id, user_id, refresh_token,
        **kwargs,
):
    return (
        encode_auth_token(
            secret, auth_ttl, now,
            login_id, user_id,
            **kwargs,
        ),
        encode_refresh_token(
            secret, refresh_ttl, refresh_nbf, now,
            login_id, refresh_token,
            **kwargs,
        ),
    )


def _decode_token(purpose, secret, token, *, decode, xid, **kwargs):
    assert decode is not None
    assert xid is not None
    claims = decode(token, secret, audience=[purpose])

    try:
        return claims['sub'], claims[xid]
    except KeyError:
        raise InvalidJWTTokenError('Token has invalid claims')


decode_auth_token = partial(_decode_token, AUTH, xid='lid')
decode_refresh_token = partial(_decode_token, REFRESH, xid='lid')
decode_email_confirmation_token = partial(_decode_token, EMAIL, xid='uid')
