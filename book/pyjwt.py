from jwt import decode, encode
from jwt.exceptions import InvalidTokenError as PyJWTInvalidTokenError

from .jwtbook import InvalidJWTTokenError


def pyjwt_encode(payload, key, **kw):
    return encode(payload, key, algorithm='HS256', **kw).decode('ascii')


def pyjwt_decode(jwt, key='', **kw):
    try:
        return decode(
            jwt, key,
            algorithms=['HS256', 'HS384', 'HS512'],
            **kw,
        )
    except PyJWTInvalidTokenError as e:
        raise InvalidJWTTokenError from e
