from .services import database_auth_factory
from .interfaces import IUserService
from .views import AccountsView


def _user(request):
    user_id = request.authenticated_userid

    if user_id is None:
        return

    return request.find_service(IUserService).get_user(user_id)


def includeme(config):
    config.register_service_factory(database_auth_factory, IUserService)

    config.add_cornice_resource(AccountsView)
    config.add_request_method(_user, 'user', reify=True)
