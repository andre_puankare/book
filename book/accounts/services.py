from passlib.context import CryptContext

from sqlalchemy.orm.exc import NoResultFound
from zope.interface import implementer

from book.accounts.interfaces import (
    IUserService,
)
from book.accounts.models import (
    User,
    Email,
    Phone,
)


@implementer(IUserService)
class DatabaseUserService:
    def __init__(self, session):
        self.db = session
        
        self.hasher = CryptContext(
            schemes=[
                "argon2",
                "bcrypt_sha256",
                "bcrypt",
                "django_bcrypt",
                "unix_disabled",
            ],
            deprecated=["auto"],
            truncate_error=True,
            # Argon 2 Configuration
            argon2__memory_cost=1024,
            argon2__parallelism=6,
            argon2__time_cost=6,
        )

    def get_user(self, userid):
        return self.db.query(User).get(userid)

    def get_user_by_username(self, username):
        user_id = self.find_user_id(username)
        return None if user_id is None else self.get_user(user_id)

    def find_user_id(self, username):
        try:
            user = self.db.query(User.id).filter(User.username == username).one()
        except NoResultFound:
            return

        return user.id

    def check_password(self, userid, password):

        user = self.get_user(userid)
        if user is not None:

            # Actually check our hash, optionally getting a new hash for it if
            # we should upgrade our saved hashed.
            ok, new_hash = self.hasher.verify_and_update(password, user.password)

            # First, check to see if the password that we were given was OK.
            if ok:
                # Then, if the password was OK check to see if we've been given
                # a new password hash from the hasher, if so we'll want to save
                # that hash.
                if new_hash:
                    user.password = new_hash
                    self.db.flush() 
                return True

        return False

    def username_is_available(self, username):
        return not self.db.query(
            self.db.query(User).filter(
                User.username == username,
            ).exists()
        ).scalar()

    def phone_is_available(self, phone):
        return not self.db.query(
            self.db.query(Phone).filter(
                Phone.phone == phone,
            ).exists()
        ).scalar()

    def email_is_available(self, email):
        return not self.db.query(
            self.db.query(Email).filter(
                Email.email == email,
            ).exists()
        ).scalar()

    def create_user(self, username, first_name, last_name,  password):
        user = User(
            username=username,
            first_name=first_name,
            last_name=last_name,
            password=self.hasher.hash(password),
        )
        self.db.add(user)
        self.db.flush()  # flush the db now so user.id is available

        return user

    def add_email(self, user_id, email_address, primary=None, verified=False):
        user = self.get_user(user_id)

        # If primary is None, then we're going to auto detect whether this should be the
        # primary address or not. The basic rule is that if the user doesn't already
        # have a primary address, then the address we're adding now is going to be
        # set to their primary.
        if primary is None:
            primary = True if user.primary_email is None else False

        email = Email(
            email=email_address, user=user, primary=primary, verified=verified
        )
        self.db.add(email)
        self.db.flush()  # flush the db now so email.id is available

        return email

    def add_phone(self, user_id, phone, primary=None, verified=False):
        user = self.get_user(user_id)

        # If primary is None, then we're going to auto detect whether this should be the
        # primary address or not. The basic rule is that if the user doesn't already
        # have a primary address, then the address we're adding now is going to be
        # set to their primary.
        if primary is None:
            primary = True if user.primary_phone is None else False

        phone = Phone(
            phone=phone, user=user, primary=primary, verified=verified
        )
        self.db.add(phone)
        self.db.flush()  # flush the db now so email.id is available

        return phone


def database_auth_factory(context, request):
    return DatabaseUserService(
        request.db,
    )

