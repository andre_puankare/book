from colander import (
    All,
    DateTime,
    Length,
    MappingSchema,
    SchemaNode,
    SequenceSchema,
    String,
    Boolean,
    Integer,
    Range,
    drop,
    required,
)

from book.utils import remove_multiple_spaces, strip_whitespace
from book.utils.colander import (
    NullDateTime,
    NullString,
    password,
    strong_password,
    username,
)


class UserID(MappingSchema):
    user_id = SchemaNode(
        Integer(),
        validator=Range(min=0, min_err="id must be greeter than 0"),
    )


class UserResponse(MappingSchema):
    id = SchemaNode(Integer())
    first_name = SchemaNode(
        String(),
        preparer=[strip_whitespace, remove_multiple_spaces],
        validator=Length(max=200),
        example='John',
    )
    last_name = SchemaNode(
        String(),
        preparer=[strip_whitespace, remove_multiple_spaces],
        validator=Length(max=200),
        example='Doe',
    )
    username = SchemaNode(
        String(),
        example='admin',
    )

    is_admin = SchemaNode(Boolean())
    is_active = SchemaNode(Boolean())
    is_verified = SchemaNode(Boolean())

    date_joined = SchemaNode(DateTime())
    date_deactivated = SchemaNode(NullDateTime())


class UserLogin(MappingSchema):
    username = SchemaNode(
        String(),
        example='admin',
        validator=username,
    )
    password = SchemaNode(
        String(),
        missing=drop,
        validator=All(password, strong_password),
        example='hackme',
    )


class UserRegister(MappingSchema):
    first_name = SchemaNode(
        String(),
        preparer=[strip_whitespace, remove_multiple_spaces],
        validator=Length(max=200),
        example='John',
    )
    last_name = SchemaNode(
        String(),
        preparer=[strip_whitespace, remove_multiple_spaces],
        validator=Length(max=200),
        example='Doe',
    )
    username = SchemaNode(
        String(),
        example='admin',
        validator=username,
    )
    password = SchemaNode(
        String(),
        missing=drop,
        validator=All(password, strong_password),
        example='hackme',
    )


class Accounts(SequenceSchema):
    account = UserResponse()
