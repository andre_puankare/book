class UserEvent:

    def __init__(
            self, user, request, user_id=None,
    ) -> None:
        if user is None:
            if user_id is None:
                raise ValueError('Either user or user_id required')

            self.user_id = user_id
        else:
            self.user = user
            self.user_id = user.id

        self.request = request
