from zope.interface import Attribute, Interface


class UserServiceError(Exception):
    pass


class UserAlreadyExistsError(UserServiceError):
    pass


class UserPhoneAlreadyExistsError(UserServiceError):
    pass


class UserEmailAlreadyExistsError(UserServiceError):
    pass


class IUserService(Interface):
    def get_user(user_id):
        """
        Return the user object that represents the given userid, or None if
        there is no user for that ID.
        """
    def find_user_id(username):
        """
        Return the user is representts the given username, or None if
        there is no user for that username.
        """

    def get_user_by_username(username):
        """
        Return the user object corresponding with the given username, or None
        if there is no user with that username.
        """

    def check_password(user_id, password, *, tags=None):
        """
        Returns a boolean representing whether the given password is valid for
        the given userid.

        May have an optional list of tags, which allows identifying the purpose of
        checking the password.
        """

    def create_user(username, first_name, last_name, password):
        """
        Accepts a user object, and attempts to create a user with those
        attributes.

        A UserAlreadyExists Exception is raised if the user already exists.
        """

    def add_email(user_id, email_address, primary=False, verified=False):
        """
        Adds an email for the provided user_id
        """

    def add_phone(user_id, phone, primary=False, verified=False):
        """
        Adds an phone for the provided user_id
        """

    def username_is_available(username):
        """
        Check if username already exists in database
        """

    def phone_is_available(phone):
        """
        Check if phone already exists in database
        """

    def email_is_available(email):
        """
        Check if email already exists in database
        """


class ITokenService(Interface):
    def dumps(data):
        """
        Generates a unique token based on the data provided
        """

    def loads(token):
        """
        Gets the data corresponding to the token provided
        """
