from book.accounts.interfaces import IUserService
from book.utils.cornice import when_valid


@when_valid
def username_is_available(request, **kwargs):
    username = request.validated.get('username')

    if not username:
        return

    user = getattr(request.context, 'user', None)

    if user is not None and user.username == username:
        return

    user_service = request.find_service(IUserService)

    if user_service.username_is_available(username) is False:
        request.errors.add('body', 'username', 'not available')


@when_valid
def phone_is_available(request, **kwargs):
    phone = request.validated.get('phone')

    if not phone:
        return

    user_service = request.find_service(IUserService)

    if user_service.phone_is_available(phone) is False:
        request.errors.add('body', 'phone', 'not available')


@when_valid
def email_is_available(request, **kwargs):
    email = request.validated.get('email')

    if not email:
        return

    user_service = request.find_service(IUserService)
    if user_service.email_is_available(email) is False:
        request.errors.add('body', 'email', 'not available')
