from sqlalchemy.orm import relationship
from sqlalchemy import (
    Column,
    Boolean,
    Integer,
    DateTime,
    String,
    ForeignKey,
    sql,
    CheckConstraint,
    UniqueConstraint,
    Index,
)
from sqlalchemy.orm.exc import NoResultFound

from book import db
from book.utils.attrs import make_repr


class UserFactory:
    def __init__(self, request):
        self.request = request
    
    def __getitem__(self, username):
        try:
            return self.request.db.query(User).filter(
                User.username == username,
            ).one()
        except NoResultFound:
            raise KeyError from None


class User(db.Model):
    __tablename__ = 'users'
    __table_args__ = (
        CheckConstraint(
            "length(username) <= 50",
            name="users_valid_username_length"),
        CheckConstraint(
            "username ~* '^([A-Z0-9]|[A-Z0-9][A-Z0-9._-]*[A-Z0-9])$'",
            name="users_valid_username",
        ),
    )
    __repr__ = make_repr("username")

    profile = relationship("Profile", uselist=False, back_populates="user")

    username = Column(String(length=256), nullable=False, unique=True)
    password = Column(String(length=128), nullable=False)

    first_name = Column(String(length=200), nullable=False)
    last_name = Column(String(length=200), nullable=False)

    is_admin = Column(Boolean(), nullable=False, default=False)
    is_active = Column(Boolean(), nullable=False, default=True)
    is_verified = Column(Boolean(), nullable=False, default=False)
    is_business = Column(Boolean(), nullable=False, default=False)

    password_date = Column(DateTime, server_default=sql.func.now())

    emails = relationship(
        'Email', backref='user', cascade='all, delete-orphan', lazy=False
    )
    phones = relationship(
        'Phone', backref='user', cascade='all, delete-orphan', lazy=False
    )

    date_joined = Column(DateTime, server_default=sql.func.now())
    date_deactivated = Column(DateTime, nullable=True)

    @property
    def primary_email(self):
        primaries = [x for x in self.emails if x.primary]
        if primaries:
            return primaries[0]

    @property
    def primary_phone(self):
        primaries = [x for x in self.phones if x.primary]
        if primaries:
            return primaries[0]


class Email(db.Model):
    __tablename__ = 'user_emails'
    __table_args__ = (
        UniqueConstraint(
            'email',
            name='user_email_email_key',
        ),
        Index(
            'user_emails_user_id',
            'user_id',
        )
    )
    __repr__ = make_repr("email")

    user_id = Column(
        Integer(),
        ForeignKey('users.id', deferrable=True, initially="DEFERRED"),
        nullable=False,
    )
    email = Column(String(length=254), nullable=False)
    primary = Column(Boolean(), nullable=False)
    verified = Column(Boolean(), nullable=False)


class Phone(db.Model):
    __tablename__ = 'user_phones'
    __table_args__ = (
        UniqueConstraint(
            'phone',
            name='user_phone_phone_key',
        ),
        Index(
            'user_phones_user_id',
            'user_id',
        ),
    )
    __repr__ = make_repr("phone")

    user_id = Column(
        Integer(),
        ForeignKey('users.id', deferrable=True, initially="DEFERRED"),
    )
    phone = Column(String(), nullable=False)
    primary = Column(Boolean(), nullable=False)
    verified = Column(Boolean(), nullable=False)
