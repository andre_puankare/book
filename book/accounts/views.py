import logging

from cornice.validators import (
    colander_path_validator,
)
from cornice.resource import resource, view

from .interfaces import IUserService
from .formatters import format_user
from .schemas import (
    UserResponse,
    UserID,
)
from book.utils.cornice import BaseResource


log = logging.getLogger(__name__)


@resource(
    path='api/users/{user_id}',
    tags=['users'],
    factory=None,
    traverse='/'
)
class AccountsView(BaseResource):
    RESPONSE_SCHEMA = UserResponse()

    @view(
        schema=UserID,
        validators=(colander_path_validator, ),
        permission='view',
    )
    def get(self):
        print(self.context)
        request = self.request
        usr_svc = request.find_service(IUserService)
        user = usr_svc.get_user(request.matchdict['user_id'])

        if user is None:
            request.errors.add('path', 'user_id', 'user not found')
            request.errors.status = 404
            return

        return self.serialize(format_user(user))
