def format_user(user):
    return {
        'id': user.id,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'username': user.username,
        'is_admin': user.is_admin,
        'is_active': user.is_active,
        'date_joined': user.date_joined,
        'is_verified': user.is_verified,
        'date_deactivated': user.date_deactivated,
    }
