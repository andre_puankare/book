import os
import enum

from functools import partial

from pyramid.config import Configurator
from pyramid.renderers import JSON
from pyramid.settings import asbool


class Environment(enum.Enum):
    production = "production"
    development = "development"


def _maybe_set(
        settings: dict,
        name: str,
        envvar: str,
        transformer=None,
        default=None,
        prefix='',
):
    """"""
    envvar = prefix + envvar
    if envvar in os.environ:
        value = os.environ[envvar]
        if transformer is not None:
            value = transformer(value)
        settings.setdefault(name, value)
    elif default is not None:
        settings.setdefault(name, default)


def configure(settings=None):
    """"""
    if settings is None:
        settings = dict()

    _maybe_set(
        settings,
        'book.env_prefix',
        'BOOK_ENV_PREFIX',
        default='',
    )

    _maybe_set(
        settings,
        'app.env',
        'BOOK_ENV',
        transformer=Environment,
        default=Environment.production,
    )
    maybe_set = partial(_maybe_set, prefix=settings['book.env_prefix'])

    maybe_set(settings, 'database.url', 'DATABASE_URL')
    maybe_set(settings, 'log.filename', 'LOG_FILENAME', default='/tmp/book.log')
    maybe_set(
        settings,
        'sqlalchemy.echo',
        'SQLALCHEMY_ECHO',
        asbool,
        default=False,
    )
    maybe_set(settings, 'celery.broker_url', 'BROKER_URL')
    maybe_set(settings, 'celery.result_url', 'REDIS_URL')
    maybe_set(settings, 'celery.scheduler_url', 'REDIS_URL')

    maybe_set(settings, 'jwt.secret', 'JWT_SECRET')
    maybe_set(
        settings,
        'jwt.auth.ttl',
        'JWT_AUTH_TTL',
        int,
        900,
    )  # 15 min
    maybe_set(
        settings,
        'jwt.email.ttl',
        'JWT_EMAIL_TTL',
        int,
        28800,
    )  # 8 hours
    maybe_set(
        settings,
        'jwt.refresh.ttl',
        'JWT_REFRESH_TTL',
        int,
        5184000,
    )  # 60 days
    maybe_set(
        settings,
        'jwt.refresh.nbf',
        'JWT_REFRESH_NBF',
        int,
        600,
    )  # 10 min
    maybe_set(settings, 'sentry.dsn', 'SENTRY_DSN')

    if settings['app.env'] == Environment.development:
        # to raw SQLAlchemy sql
        settings.setdefault('pyramid_reload_assets', True)
        settings.setdefault('pyramid.reload_templates', True)
        settings.setdefault('pyramid.prevent_http_cache', True)
        settings.setdefault('debugtoolbar.hosts', ['0.0.0.0/0'])
        settings.setdefault(
            'debugtoolbar.panels',
            [
                '.'.join(['pyramid_debugtoolbar.panels', panel])
                for panel in [
                    'versions.VersionDebugPanel',
                    'settings.SettingsDebugPanel',
                    'headers.HeaderDebugPanel',
                    'request_vars.RequestVarsDebugPanel',
                    'renderings.RenderingsDebugPanel',
                    'logger.LoggingPanel',
                    'performance.PerformanceDebugPanel',
                    'routes.RoutesDebugPanel',
                    'sqla.SQLADebugPanel',
                    'tweens.TweensDebugPanel',
                    'introspection.IntrospectionDebugPanel',
                ]
            ],
        )

    config = Configurator(settings=settings,
                          root_factory='.traverse.Root')

    # Register support for API
    config.include('cornice')
    # Register API Docs
    config.include('cornice_swagger')

    config.add_renderer('json', JSON(
        ensure_ascii=False,
        indent=2,
    ))

    # Transaction manager
    config.include('pyramid_tm')

    # Templates renderer
    config.include('pyramid_jinja2')
    # We also want to use Jinja2 for .html templates as well, because we just
    # assume that all templates will be using Jinja.
    config.add_jinja2_renderer('.html')

    # Sometimes our files are .txt files and we still want to use Jinja2 to
    # render them.
    config.add_jinja2_renderer('.txt')

    # Anytime we want to render a .xml template, we'll also use Jinja.
    config.add_jinja2_renderer('.xml')

    # We'll store all of our templates in one location, book/templates
    # so we'll go ahead and add that to the Jinja2 search path.
    config.add_jinja2_search_path('book:templates', name='.html')
    config.add_jinja2_search_path('book:templates', name='.txt')
    config.add_jinja2_search_path('book:templates', name='.xml')

    # Register support for JsonRPC
    config.include('pyramid_rpc.jsonrpc')

    # Register support for services
    config.include('pyramid_services')

    if config.registry.settings['app.env'] == Environment.development:
        config.include('pyramid_debugtoolbar')

    config.include('.logg')
    config.include('.routes')
    config.include('.db')
    config.include('.auth')
    config.include('.accounts')
    config.include('.sentry')

    config.scan(
        ignore=[
            'book.migrations.env',
            'book.celery',
            'book.wsgi',
        ],
    )

    config.commit()

    return config

