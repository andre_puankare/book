import logging

import sqlalchemy
import zope.sqlalchemy

import alembic.config

from sqlalchemy import Integer
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import OperationalError


logger = logging.getLogger(__name__)


metadata = sqlalchemy.MetaData()


class ModelBase:
    def __repr__(self):
        inst = inspect(self)
        self.__repr__ = make_repr(
            *[c_attr.key for c_attr in inst.mapper.column_attrs], _self=self
        )
        return self.__repr__()


# Base class for models using declarative syntax
ModelBase = declarative_base(cls=ModelBase, metadata=metadata)


class Model(ModelBase):

    __abstract__ = True

    id = sqlalchemy.Column(
        Integer(),
        primary_key=True,
    )


Session = sessionmaker()


class DatabaseNotAvailable(Exception):
    ...


def _configure_alembic(config):
    alembic_cfg = alembic.config.Config()
    alembic_cfg.set_main_option('script_location', 'book:migrations')
    alembic_cfg.set_main_option('url', config.registry.settings['database.url'])
    return alembic_cfg


def _create_engine(url, echo):
    return sqlalchemy.create_engine(
        url,
        pool_size=40,
        max_overflow=60,
        pool_timeout=30,
        echo=echo,
        encoding='utf8',
        client_encoding='utf8',
    )


def _create_session(request):
    try:
        connection = request.registry['sqlalchemy.engine'].connect()
    except OperationalError:
        logger.warning('Got an error connecting to PostgreSQL', exc_info=True)
        raise DatabaseNotAvailable()

    session = Session(bind=connection)

    zope.sqlalchemy.register(session, transaction_manager=request.tm)

    @request.add_finished_callback
    def cleanup(request):
        session.close()
        connection.close()

    return session


def includeme(config):
    config.add_directive('alembic_config', _configure_alembic)

    # Create SQLAlchemy Engine
    config.registry['sqlalchemy.engine'] = _create_engine(
        config.registry.settings['database.url'],
        echo=config.registry.settings['sqlalchemy.echo'],
    )
    config.add_request_method(_create_session, name="db", reify=True)
