import sentry_sdk

from sentry_sdk.integrations.pyramid import PyramidIntegration


def includeme(config):
    sentry_sdk.init(
     dsn=config.registry.settings['sentry.dsn'],
     integrations=[PyramidIntegration()]
    )
