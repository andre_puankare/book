import logging.config


def includeme(config):
    config_listener = {
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'detailed': {
                'class': 'logging.Formatter',
                'format': '%(asctime)s %(name)-15s %(levelname)-8s %(processName)-10s %(message)s'
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': 'WARNING',
                'formatter': 'detailed',
            },
            'file': {
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': config.registry.settings['log.filename'],
                'maxBytes': 1024 * 1024 * 64,  # 64MB per log file
                'backupCount': 9,  # Total 64 * 9 = 576
                'mode': 'a',
                'formatter': 'detailed',
            },
        },
        'root': {
            'level': 'WARNING',
            'handlers': ['console', 'file']
        },
    }
    logging.config.dictConfig(config_listener)
