import re

from pyramid.security import (
    Deny,
    Allow,
    Authenticated,
    ALL_PERMISSIONS
)


from traversalkit import DEC_ID, Resource

from book.utils.cornice import ServiceDescriptor
from book.accounts.interfaces import IUserService


USER_ID = re.compile(r'^(\d+|me)$')


class XResource(Resource):

    def __getitem__(self, name):
        return self.get(name, payload=self.request)

    def on_init(self, payload):
        self.request = payload

        if hasattr(self, 'xinit'):
            self.xinit()

    @classmethod
    def factory(cls, request):
        return cls(payload=request)


class Root(XResource):
    __acl__ = [
        (Deny, 'group:inactive', ALL_PERMISSIONS),
        (Allow, 'group:admins', ALL_PERMISSIONS),
        (Allow, Authenticated, 'view'),
    ]

    def __init__(self, request, context=None):
        self.request = request
        self.context = context


@Root.mount('users')
class Users(XResource):
    pass


@Users.mount_set(USER_ID, metaname='user_id')
class User(XResource):
    usr_svc = ServiceDescriptor(IUserService)

    def __acl__(self):
        return [
            (Allow, str(self.user.id), ('edit', 'view')),
        ]

    def xinit(self):
        if self.__name__ == 'me':
            user = self.request.user
        else:
            user = self.usr_svc.get(int(self.__name__))

        self.user = user

        if user is None:
            raise KeyError(self.__name__, self.uri)