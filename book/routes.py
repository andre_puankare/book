from pyramid.security import NO_PERMISSION_REQUIRED


def includeme(config):
    config.add_route('health', '/_health/')
    config.add_route('echo', '/_echo/')
    config.add_route('root', '/')
    config.add_route('cornice_swagger.open_api_path', '/swagger.json')
    config.add_route('cornice_swagger.api_explorer_path', '/_api/')
    config.add_view(
        'cornice_swagger.views.swagger_ui_template_view',
        permission=NO_PERMISSION_REQUIRED,
        route_name='cornice_swagger.api_explorer_path',
    )
    config.add_jsonrpc_endpoint('rpc', '/rpc')
    config.add_static_view(
        name='static',
        path='book:static',
        cache_max_age=3600,
    )

