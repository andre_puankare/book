package main

import (
	"log"
	"os"

	"fmt"

	accountRest "bitbucket.org/andre_puankare/book/internal/account/delivery/rest"
	accountRepositories "bitbucket.org/andre_puankare/book/internal/account/repositories"
	accountUsecases "bitbucket.org/andre_puankare/book/internal/account/usecases"
	healthRest "bitbucket.org/andre_puankare/book/internal/health/delivery/rest"
	healthGRPC "bitbucket.org/andre_puankare/book/internal/health/delivery/rpc"

	config "bitbucket.org/andre_puankare/book/internal/config"
	dbcontext "bitbucket.org/andre_puankare/book/pkg/dbcontext"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"gopkg.in/alecthomas/kingpin.v2"
)

// Version indicates the current version of the application.
var Version = "v0.1.1"

const (
	RESTService = "rest"
	gRPCService = "grpc"
)

var (
	debug   = kingpin.Flag("debug", "Debug mode.").Short('d').Bool()
	version = kingpin.Flag("version", "Show version.").Short('v').Bool()
	name    = kingpin.Flag("name", "Show project name.").Short('n').Bool()
	service = kingpin.Flag("service", "Start service [rest,grpc]. default rest").Short('s').Enum(
		RESTService,
		gRPCService,
	)
	clientGrpc = kingpin.Flag("client", "Client rpc").Short('r').Bool()
)

func main() {
	kingpin.Parse()

	if *version {
		fmt.Println(Version)
		os.Exit(0)
	}
	if *name {
		fmt.Println("book")
		os.Exit(0)
	}

	// load application configurations
	cfg, err := config.Load()
	if err != nil {
		os.Exit(-1)
	}

	switch *service {
	case RESTService:
		startRESTService(cfg)
	case gRPCService:
		startgRPCService()
	default:
		startRESTService(cfg)
	}
}

func startgRPCService() {
	if *clientGrpc {
		fmt.Println("client gRPC")
		healthGRPC.RunClient()
	} else {
		fmt.Println("server gRPC")
		healthGRPC.RunGrpc()
	}
}

func startRESTService(cfg *config.Config) {
	// connect to the database
	db, err := dbcontext.GetDB(cfg.DBURI)
	if err != nil {
		os.Exit(-1)
	}
	defer func() {
		dbcontext.CloseDB()
	}()

	// build HTTP server
	e := echo.New()
	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	apiV1 := e.Group("/v1")

	accUC := accountUsecases.NewAccountUsecase(accountRepositories.NewAccountRepository(db))

	googleAuthUC := accountUsecases.NewGoogleAuthUsecase(
		accountRepositories.NewGoogleAuthRepository(
			cfg.GoogleOauthConfig,
			cfg.GoogleOauthStateString,
		))
	accountRest.NewAccountHandler(apiV1, accUC)
	accountRest.NewAuthHandler(apiV1, accUC, googleAuthUC)
	healthRest.NewHealthHandler(e)

	address := fmt.Sprintf(":%d", cfg.ServerPort)
	log.Fatal(e.Start(address))
}
