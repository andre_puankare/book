#!/bin/bash

set -e

if [ -n "$EXTRA_PATH" ]; then
    export PATH="$EXTRA_PATH:$PATH"
fi

BIN="$(dirname "$(realpath "$0")")"
ROOT="$(dirname "$BIN")"

case "$1" in
    tests)
        shift
        make -C "$ROOT" test
    ;;
    dev-server)
        shift
        exec make -C "$ROOT" dev-server 
    ;;
    prod-server)
        shift
        exec make -C "$ROOT" prod-server
    ;;
    *)
        shift
        exec make -C "$ROOT" prod-server
esac
