package dbcontext

import (
	accountEntities "bitbucket.org/andre_puankare/book/internal/account/entities"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB

// DB Wrapped db instance
type DB struct {
	db *gorm.DB
}

// CloseDB close connection to db
func CloseDB() {
	db.Close()
}

// GetDB returns a new DB connection that wraps the given dbx.DB instance.
func GetDB(dbURI string) (*gorm.DB, error) {
	conn, err := gorm.Open("postgres", dbURI)
	if err != nil {
		return nil, err
	}
	db = conn

	db.Debug().AutoMigrate(&accountEntities.Account{})

	return db, nil
}
