package rest

import (
	validator "gopkg.in/go-playground/validator.v9"
)

// ValidationResponse represent validation error message
type ValidationResponse struct {
	Location string
	Errors   []struct {
		Field  string
		Reason string
	}
}

// FromValidationErrors creates structured info
func (v ValidationResponse) FromValidationErrors(e error, location string) ValidationResponse {
	v.Location = location
	for _, err := range e.(validator.ValidationErrors) {
		v.Errors = append(v.Errors, struct {
			Field  string
			Reason string
		}{
			Field:  err.Field(),
			Reason: err.ActualTag(),
		})
	}
	return v
}
