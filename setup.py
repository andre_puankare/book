import os
from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()


def extras_require():
    for name in os.listdir(os.path.join(here, 'requirements')):
        if not name.endswith('.in') or name == 'main.in':
            continue

        with open(os.path.join(here, 'requirements', name), 'rt') as f:
            yield name[:-3], f.readlines()


def install_requires():
    with open(os.path.join(here, 'requirements', 'main.in'), 'rt') as f:
        return f.readlines()


setup(
    name='book',
    version='0.1.1',
    description='booker',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Pyramid',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
    author='Yushin Andrey',
    author_email='baybaraandrey@gmail.com',
    url='',
    keywords='web pyramid pylons',
    packages=find_packages(
        include=['book', 'book.*'],
        exclude=['tests', 'tests.*'],
    ),
    include_package_data=True,
    zip_safe=False,
    extras_require=dict(extras_require()),
    install_requires=install_requires(),
    entry_points={
        'console_scripts': [
            'book=book.cli:book',
        ],
    },
)
