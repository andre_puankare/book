BINARY=engine
NAME=book
VERSION=v0.1.1
test: 
	go test -v -cover -covermode=atomic ./...

engine:
	@go build -o ${BINARY} cmd/main.go

unittest:
	go test -short  ./...

clean:
	if [ -f ${BINARY} ] ; then rm ${BINARY} ; fi

docker:
	docker build -t book/${NAME}:${VERSION} -t  book/${NAME}:latest .

docker-push: docker
	./cmd/build_image.sh version

version: engine
	@./engine --version

name: engine
	@./engine --name

run:
	docker-compose up

stop:
	docker-compose down

dev-prepare:
	@echo "Prepare local dev tools"
	go get ./...
	go get github.com/codegangsta/gin
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s latest

dev-server: engine
	gin -a 8080 -p 3000  -i --all -b engine  -d cmd/ run
	
prod-server: engine
	@./engine

lint-prepare:
	@echo "Installing golangci-lint" 
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s latest

lint:
	./bin/golangci-lint run \
		--exclude-use-default=false \
		--enable=golint \
		--enable=gocyclo \
		--enable=goconst \
		--enable=unconvert \
		./...

.PHONY: clean install unittest build docker run stop vendor lint-prepare lint
